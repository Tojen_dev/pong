﻿using UnityEngine;

namespace Extensions
{
    public static class RigidbodyExtension
    {
        #region MovePosition

        public static void MovePositionX(this Rigidbody rigidbody, float value)
        {
            Vector3 position = rigidbody.position;
            position.x = value;
            rigidbody.MovePosition(position);
        }

        public static void MovePositionY(this Rigidbody rigidbody, float value)
        {
            Vector3 position = rigidbody.position;
            position.y = value;
            rigidbody.MovePosition(position);
        }

        public static void MovePositionZ(this Rigidbody rigidbody, float value)
        {
            Vector3 position = rigidbody.position;
            position.z = value;
            rigidbody.MovePosition(position);
        }

        #endregion

        #region Velocity

        public static void SetVelocityX(this Rigidbody rigidbody, float value)
        {
            Vector3 velocity = rigidbody.velocity;
            velocity.x = value;
            rigidbody.velocity = velocity;
        }

        public static void SetVelocityY(this Rigidbody rigidbody, float value)
        {
            Vector3 velocity = rigidbody.velocity;
            velocity.y = value;
            rigidbody.velocity = velocity;
        }

        public static void SetVelocityZ(this Rigidbody rigidbody, float value)
        {
            Vector3 velocity = rigidbody.velocity;
            velocity.z = value;
            rigidbody.velocity = velocity;
        }

        #endregion

        #region Positions

        public static void SetX(this Rigidbody rigidbody, float value)
        {
            Vector3 temp = rigidbody.position;
            temp.x = value;
            rigidbody.position = temp;
        }

        public static void SetY(this Rigidbody rigidbody, float value)
        {
            Vector3 temp = rigidbody.position;
            temp.y = value;
            rigidbody.position = temp;
        }

        public static void SetZ(this Rigidbody rigidbody, float value)
        {
            Vector3 temp = rigidbody.position;
            temp.z = value;
            rigidbody.position = temp;
        }

        #endregion
    }
}