using UnityEngine;

namespace Extensions
{
    public static class Rigidbody2DExtension
    {
        #region Velocity

        public static void SetVelocityX(this Rigidbody2D rigidbody, float value)
        {
            Vector2 velocity = rigidbody.velocity;
            velocity.x = value;
            rigidbody.velocity = velocity;
        }

        public static void SetVelocityY(this Rigidbody2D rigidbody, float value)
        {
            Vector2 velocity = rigidbody.velocity;
            velocity.y = value;
            rigidbody.velocity = velocity;
        }

        #endregion

        #region MovePosition

        public static void MovePositionX(this Rigidbody2D rigidbody, float value)
        {
            Vector2 position = rigidbody.position;
            position.x = value;
            rigidbody.position = position;
        }

        public static void MovePositionY(this Rigidbody2D rigidbody, float value)
        {
            Vector2 position = rigidbody.position;
            position.y = value;
            rigidbody.position = position;
        }

        #endregion
    }
}