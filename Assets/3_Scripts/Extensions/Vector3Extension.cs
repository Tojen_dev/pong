using UnityEngine;

namespace Extensions
{
    public static class Vector3Extension
    {
        public static void Abs(this ref Vector3 value)
        {
            value.x = Mathf.Abs(value.x);
            value.y = Mathf.Abs(value.y);
            value.z = Mathf.Abs(value.z);
        }

        public static Vector2Int FloorToInt2(this Vector3 value)
        {
            return new Vector2Int
            {
                x = Mathf.FloorToInt(value.x),
                y = Mathf.FloorToInt(value.z)
            };
        }

        public static Vector3Int FloorToInt3(this Vector3 value)
        {
            return new Vector3Int
            {
                x = Mathf.FloorToInt(value.x),
                y = Mathf.FloorToInt(value.y),
                z = Mathf.FloorToInt(value.z)
            };
       }
    }
}