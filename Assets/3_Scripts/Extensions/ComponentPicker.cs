﻿using UnityEngine;

namespace Extensions
{
    public static class ComponentPicker
    {
        #region Collider

        public static bool TryGetComponent<T>(this Collider collider, PickPlace pickPlace, out T component)
            where T : class
        {
            component = collider.GetComponent<T>(pickPlace);
            return component != null;
        }

        public static T GetComponent<T>(this Collider container, PickPlace pickPlace) where T : class
        {
            switch (pickPlace)
            {
                case PickPlace.Self:
                    return container.GetComponent<T>();

                case PickPlace.AttachedRigidbody:
                    return container.attachedRigidbody?.GetComponent<T>();

                case PickPlace.Parent:
                    return container.GetComponentInParent<T>();

                case PickPlace.Children:
                    return container.GetComponentInChildren<T>();

                default:
                    return null;
            }
        }

        #endregion

        #region GameObject

        public static bool TryGetComponent<T>(this GameObject gameObject, PickPlace pickPlace, out T component)
            where T : class
        {
            component = gameObject.GetComponent<T>(pickPlace);
            return component != null;
        }

        public static T GetComponent<T>(this GameObject gameObject, PickPlace pickPlace) where T : class
        {
            switch (pickPlace)
            {
                case PickPlace.Self:
                    return gameObject.GetComponent<T>();

                case PickPlace.Parent:
                    return gameObject.GetComponentInParent<T>();

                case PickPlace.Children:
                    return gameObject.GetComponentInChildren<T>();

                default:
                    return null;
            }
        }

        #endregion

        #region Component

        public static bool TryGetComponent<T, C>(this T container, PickPlace pickPlace, out C component)
            where T : Component where C : class
        {
            component = container.GetComponent<T, C>(pickPlace);
            return false;
        }


        public static C GetComponent<T, C>(this T container, PickPlace pickPlace) where T : Component where C : class
        {
            switch (pickPlace)
            {
                case PickPlace.Self:
                    return container.GetComponent<C>();

                case PickPlace.Parent:
                    return container.GetComponentInParent<C>();

                case PickPlace.Children:
                    return container.GetComponentInChildren<C>();

                default:
                    return null;
            }
        }

        #endregion
    }


    public enum PickPlace
    {
        Self,
        Parent,
        Children,
        AttachedRigidbody,
    }
}