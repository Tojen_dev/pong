using UnityEngine;

namespace Extensions
{
    public static class ArrayExtension
    {
        public static Vector2Int GetSize<T>(this T[,] array)
        {
            int rows = array.GetUpperBound(0) + 1;
            int columns = array.Length / rows;

            return new Vector2Int(columns, rows);
        }
    }
}