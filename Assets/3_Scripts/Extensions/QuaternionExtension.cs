﻿using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

namespace Extensions
{
    public static class QuaternionExtension
    {
        public static Quaternion LookAtY(Vector3 position, Vector3 targetPosition)
        {
            Vector3 relativePosition = targetPosition - position;
            Vector3 eulerAngles = Quaternion.LookRotation(relativePosition, Vector3.up).eulerAngles;
            Quaternion result = Quaternion.Euler(0, eulerAngles.y, 0);

            return result;
        }

        public static Quaternion LookAtY(this Transform transform, Transform target)
        {
            Vector3 relativePosition = target.position - transform.position;
            Vector3 eulerAngles = Quaternion.LookRotation(relativePosition, Vector3.up).eulerAngles;
            Quaternion result = Quaternion.Euler(0, eulerAngles.y, 0);

            return result;
        }
    }
}