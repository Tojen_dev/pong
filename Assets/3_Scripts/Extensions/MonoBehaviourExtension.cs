using UnityEngine;

namespace Extensions
{
    public static class MonoBehaviourExtension
    {
        public static void Show(this MonoBehaviour mono) => mono.gameObject.SetActive(true);
        public static void Hide(this MonoBehaviour mono) => mono.gameObject.SetActive(false);
    }
}