﻿using UnityEngine;

namespace Extensions
{
    public static class TransformExtension
    {
        #region Positions

        public static void SetX(this Transform transform, float value)
        {
            Vector3 temp = transform.position;
            temp.x = value;
            transform.position = temp;
        }

        public static void SetY(this Transform transform, float value)
        {
            Vector3 temp = transform.position;
            temp.y = value;
            transform.position = temp;
        }

        public static void SetZ(this Transform transform, float value)
        {
            Vector3 temp = transform.position;
            temp.z = value;
            transform.position = temp;
        }


        public static void SetLocalX(this Transform transform, float value)
        {
            Vector3 temp = transform.localPosition;
            temp.x = value;
            transform.localPosition = temp;
        }

        public static void SetLocalY(this Transform transform, float value)
        {
            Vector3 temp = transform.localPosition;
            temp.y = value;
            transform.localPosition = temp;
        }

        public static void SetLocalZ(this Transform transform, float value)
        {
            Vector3 temp = transform.localPosition;
            temp.z = value;
            transform.localPosition = temp;
        }

        #endregion


        #region Scale

        public static void SetLocalScaleX(this Transform transform, float value)
        {
            Vector3 scale = transform.localScale;
            scale.x = value;
            transform.localScale = scale;
        }

        public static void SetLocalScaleY(this Transform transform, float value)
        {
            Vector3 scale = transform.localScale;
            scale.y = value;
            transform.localScale = scale;
        }

        public static void SetLocalScaleZ(this Transform transform, float value)
        {
            Vector3 scale = transform.localScale;
            scale.z = value;
            transform.localScale = scale;
        }

        #endregion


        #region WidthDepthDifference

        public static Vector3 WidthDepthDifference(this Vector3 first, Vector3 second)
        {
            Vector3 firstPosition = first;
            Vector3 secondPosition = second;
            firstPosition.y = 0;
            secondPosition.y = 0;

            return firstPosition - secondPosition;
        }

        public static Vector3 WidthDepthDifference(this Transform first, Vector3 second)
        {
            Vector3 firstPosition = first.position;
            Vector3 secondPosition = second;
            firstPosition.y = 0;
            secondPosition.y = 0;

            return firstPosition - secondPosition;
        }

        #endregion


        #region Rotate

        public static Vector3 RotateVector3(Vector3 direction, float angle)
        {
            float x = direction.x * Mathf.Cos(angle * Mathf.Deg2Rad) - direction.z * Mathf.Sin(angle * Mathf.Deg2Rad);
            float z = direction.x * Mathf.Sin(angle * Mathf.Deg2Rad) + direction.z * Mathf.Cos(angle * Mathf.Deg2Rad);

            Vector3 result = new Vector3(x, 0, z);
            return result;
        }

        public static Vector3 RotateVector3AroundY(Vector3 pointPosition, Vector3 pivotPosition, float angle)
        {
            angle *= Mathf.Deg2Rad;

            Vector3 result = pivotPosition + new Vector3
            {
                x = Mathf.Cos(angle) * (pointPosition.x - pivotPosition.x) -
                    Mathf.Sin(angle) * (pointPosition.z - pivotPosition.z),
                y = pointPosition.y,
                z = Mathf.Sin(angle) * (pointPosition.x - pivotPosition.x) +
                    Mathf.Cos(angle) * (pointPosition.z - pivotPosition.z)
            };

            return result;
        }

        public static Vector3 RotateAroundY(Transform point, Transform pivot, float angle)
        {
            return RotateVector3AroundY(point.position, pivot.position, angle);
        }


        // function RotatePointAroundPivot(point: Vector3, pivot: Vector3, angles: Vector3): Vector3 {
        //     var dir: Vector3 = point - pivot; // get point direction relative to pivot
        //     dir = Quaternion.Euler(angles) * dir; // rotate it
        //     point = dir + pivot; // calculate rotated point
        //     return point; // return it
        // }


        // public static Vector3 RotateAroundByX(Vector3 center, Vector3 point, float angle)
        // {
        //     Vector3 result = new Vector3()
        //                      {
        //                          x = center.x;
        //                          y * Mathf.
        //                          x = 
        //                      };
        //
        // |1 0 0| |x|   |        x        |   |x'|
        //         |0   cos θ    −sin θ| |y| = |y cos θ − z sin θ| = |y'|
        //         |0   sin θ     cos θ| |z|   |y sin θ + z cos θ|   |z'|
        // }

        #endregion


        #region Euler

        public static void SetLocalEulerX(this Transform transform, float value)
        {
            Vector3 temp = transform.localEulerAngles;
            temp.x = value;
            transform.localEulerAngles = temp;
        }

        public static void SetLocalEulerY(this Transform transform, float value)
        {
            Vector3 temp = transform.localEulerAngles;
            temp.x = value;
            transform.localEulerAngles = temp;
        }

        public static void SetLocalEulerZ(this Transform transform, float value)
        {
            Vector3 temp = transform.localEulerAngles;
            temp.x = value;
            transform.localEulerAngles = temp;
        }

        #endregion

        public static Transform[] GetChildren(this Transform transform)
        {
            Transform[] result = new Transform[transform.childCount];

            for (int i = 0; i < transform.childCount; i++)
                result[i] = transform.GetChild(i);

            return result;
        }

        public static void Reset(this Transform transform)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
    }
}