﻿using UnityEngine;

namespace Extensions
{
    public static class RectTransformExtension
    {
        #region Positions

        public static void SetAnchoredX(this RectTransform rectTransform, float value)
        {
            Vector3 temp = rectTransform.anchoredPosition;
            temp.x = value;
            rectTransform.anchoredPosition = temp;
        }

        public static void SetAnchoredY(this RectTransform rectTransform, float value)
        {
            Vector3 temp = rectTransform.anchoredPosition;
            temp.y = value;
            rectTransform.anchoredPosition = temp;
        }

        #endregion


        #region Size Delta

        public static void SetSizeDeltaX(this RectTransform rectTransform, float value)
        {
            Vector2 sizeDelta = rectTransform.sizeDelta;
            sizeDelta.x = value;
            rectTransform.sizeDelta = sizeDelta;
        }

        public static void SetSizeDeltaY(this RectTransform rectTransform, float value)
        {
            Vector2 sizeDelta = rectTransform.sizeDelta;
            sizeDelta.y = value;
            rectTransform.sizeDelta = sizeDelta;
        }

        #endregion
    }
}