﻿using UnityEngine;

namespace Extensions
{
    public static class AnimatorExtension
    {
        public static void SetTrigger(this Animator animator, int triggerHash, bool resetAll = true)
        {
            if (resetAll)
                ResetAllTriggers(animator);

            animator.SetTrigger(triggerHash);
        }

        public static void SetTrigger(this Animator animator, string trigger, bool resetAll = true)
        {
            if (resetAll)
                ResetAllTriggers(animator);

            animator.SetTrigger(trigger);
        }


        public static void ResetAllTriggers(this Animator animator)
        {
            for (var i = 0; i < animator.parameters.Length; i++)
            {
                AnimatorControllerParameter parameter = animator.parameters[i];
                if (parameter.type == AnimatorControllerParameterType.Trigger)
                    animator.ResetTrigger(parameter.name);
            }
        }
    }
}