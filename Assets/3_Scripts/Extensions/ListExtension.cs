﻿using System.Collections.Generic;
using UnityEngine;

namespace Extensions
{
    public static class ListExtension
    {
        public static void FillByIndexes(this List<int> list, int count)
        {
            list.Clear();
            
            for (int i = 0; i < count; i++)
                list.Add(i);
        }
    }
}