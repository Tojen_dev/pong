﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Test
{
    public class MethodRunner : MonoBehaviour
    {
        [SerializeField]
        private KeyMethodPair[] bindings;


        [Header("Update Run:")]
        [SerializeField]
        private UnityEvent updateMethod;


        public void Update()
        {
            foreach (var pair in bindings)
            {
                if (Input.GetKeyDown(pair.activationKey))
                    pair.method?.Invoke();
            }

            updateMethod?.Invoke();
        }


        #region Inner

        [Serializable]
        public class KeyMethodPair
        {
            public KeyCode activationKey;

            [Space]
            public UnityEvent method;
        }

        #endregion
    }
}