﻿using UnityEngine;

namespace _3_Scripts.Utilities
{
    public static class CollisionUtility 
    {
        public static T GetComponentOnRigidbody<T>(this Collider collider) where T : MonoBehaviour
        {
            if (collider.attachedRigidbody == null)
                return null;

            T result = collider.attachedRigidbody.GetComponent<T>();
            return result;
        }
    }
}
