﻿using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public class TransformSaver
    {
        private readonly Dictionary<Transform, TransformValues> _defaultValues =
            new Dictionary<Transform, TransformValues>();


        #region Save

        public void Save(params Transform[] transforms)
        {
            if (transforms == null)
                return;

            for (var i = 0; i < transforms.Length; i++)
                Save(transforms[i]);
        }

        public void Save(Transform transform)
        {
            if (_defaultValues.ContainsKey(transform) == false)
                _defaultValues.Add(transform, null);

            _defaultValues[transform] = new TransformValues(transform.position, transform.rotation);
        }

        
        public void SaveLocal(params Transform[] transforms)
        {
            if (transforms == null)
                return;

            for (var i = 0; i < transforms.Length; i++)
                SaveLocal(transforms[i]);
        }
        
        public void SaveLocal(Transform transform)
        {
            if (_defaultValues.ContainsKey(transform) == false)
                _defaultValues.Add(transform, null);

            _defaultValues[transform] = new TransformValues(transform.localPosition, transform.localRotation);
        }

        #endregion


        #region Load

        public void Load(params Transform[] transforms)
        {
            if (transforms == null)
                return;

            for (var i = 0; i < transforms.Length; i++)
                Load(transforms[i]);
        }

        public void Load(Transform transform)
        {
            _defaultValues.TryGetValue(transform, out TransformValues from);

            if (from != null)
            {
                transform.position = from.Position;
                transform.rotation = from.Rotation;
            }
        }

        
        public void LoadLocal(params Transform[] transforms)
        {
            if (transforms == null)
                return;

            for (var i = 0; i < transforms.Length; i++)
                LoadLocal(transforms[i]);
        }

        public void LoadLocal(Transform transform)
        {
            _defaultValues.TryGetValue(transform, out TransformValues from);

            if (from != null)
            {
                transform.localPosition = from.Position;
                transform.localRotation = from.Rotation;
            }
        }

        #endregion


        private class TransformValues
        {
            public readonly Vector3 Position;
            public readonly Quaternion Rotation;

            public TransformValues(Vector3 position, Quaternion rotation)
            {
                Position = position;
                Rotation = rotation;
            }
        }
    }
}