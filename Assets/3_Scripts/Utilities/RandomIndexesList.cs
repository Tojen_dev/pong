﻿using System.Collections.Generic;
using UnityEngine;

namespace Others
{
    public class RandomIndexesList
    {
        private readonly List<int> _indexes;


        public int Count => _indexes.Count;


        public RandomIndexesList(int count)
        {
            _indexes = new List<int>();

            for (int i = 0; i < count; i++)
                _indexes.Add(i);
        }


        public int Value
        {
            get
            {
                if (_indexes.Count == 0)
                    return -1;

                int index = Random.Range(0, _indexes.Count);
                int result = _indexes[index];

                _indexes.RemoveAt(index);

                return result;
            }
        }

        public int GetByIndex(int index)
        {
            if (index < 0 || index >= _indexes.Count)
                return Value;

            int result = _indexes[index];
            _indexes.RemoveAt(index);

            return result;
        }

        public int GetByIndex(int index, int[] infoSkipSpawnPoints)
        {
            int result;
            if (index < 0 || index >= _indexes.Count)
            {
                index = Random.Range(0, _indexes.Count);
                result = _indexes[index];

                while (Contains(infoSkipSpawnPoints, result))
                {
                    index = Random.Range(0, _indexes.Count);
                    result = _indexes[index];
                }

                _indexes.RemoveAt(index);

                return result;
            }

            result = _indexes[index];
            _indexes.RemoveAt(index);

            return result;
        }

        private bool Contains(int[] indexes, int index)
        {
            for (var i = 0; i < indexes.Length; i++)
            {
                if (indexes[i] == index)
                    return true;
            }

            return false;
        }

        public void RemoveValues(List<int> indexes)
        {
            for (int i = 0; i < indexes.Count; i++)
                _indexes.Remove(indexes[i]);
        }
    }
}