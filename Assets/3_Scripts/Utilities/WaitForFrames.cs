﻿using UnityEngine;

namespace Utilities
{
    public class WaitForFrames : CustomYieldInstruction
    {
        private int _frame;
        private readonly int _frames;


        public WaitForFrames(int frames)
        {
            _frames = frames;
        }

        public override bool keepWaiting
        {
            get
            {
                return ++_frame < _frames;
            }
        }

        public new void Reset()
        {
            _frame = 0;
        }
    }
}