﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Controllers
{
    public class InputHandler : MonoBehaviour
    {
        public static event Action<InputResult> OnDown;
        public static event Action<InputResult> OnDrag;
        public static event Action<InputResult> OnUp;


        [SerializeField]
        private float startMoveShift;

        [SerializeField]
        private float startHoldTime;

        [Space]
        [SerializeField]
        private CanvasScaler scaler;

        private float _heightScaleFactor;
        private InputResult _inputResult;


        private void Awake()
        {
            float ratio = (float)Screen.height / Screen.width;
            _heightScaleFactor = Screen.height / scaler.referenceResolution.y;
            _inputResult = new InputResult(_heightScaleFactor);
        }


        public void Update()
        {
            if (Input.GetMouseButtonDown(0))
                Down();

            else if (Input.GetMouseButton(0))
                Drag();

            else if (Input.GetMouseButtonUp(0))
                Up();
        }


        private void Down()
        {
            _inputResult.State = InputState.None;

            _inputResult.Time.Start = Time.time;
            _inputResult.Time.Now = Time.time;

            _inputResult.Position.Down = Input.mousePosition;
            _inputResult.Position.Current = Input.mousePosition;
            _inputResult.Position.Previous = Input.mousePosition;

            OnDown?.Invoke(_inputResult);
        }

        private void Drag()
        {
            _inputResult.Time.Now = Time.time;

            _inputResult.Position.Previous = _inputResult.Position.Current;
            _inputResult.Position.Current = Input.mousePosition;

            TryUpdateState();

            OnDrag?.Invoke(_inputResult);
        }

        private void Up()
        {
            _inputResult.Time.Now = Time.time;

            _inputResult.Position.Previous = _inputResult.Position.Current;
            _inputResult.Position.Current = Input.mousePosition;

            TryUpdateState();

            OnUp?.Invoke(_inputResult);
        }


        private void TryUpdateState()
        {
            if (_inputResult.State == InputState.Moved)
                return;

            if (_inputResult.DeltaDownNormalized.sqrMagnitude > Mathf.Pow(startMoveShift, 2))
                _inputResult.State = InputState.Moved;

            if (_inputResult.State == InputState.None && _inputResult.Time.Elapsed > startHoldTime)
                _inputResult.State = InputState.Hold;
        }
    }


    public struct InputResult
    {
        public InputState State;

        public InputTime Time;
        public InputPosition Position;

        public Vector2 DeltaDown => Position.Current - Position.Down;
        public Vector2 DeltaDownNormalized => DeltaDown / Screen.width;

        public Vector2 DeltaFrame => Position.Current - Position.Previous;
        public Vector2 DeltaFrameTransformed => DeltaFrame / _heightScaleFactor;
        public Vector2 DeltaFrameNormalized => DeltaFrame / Screen.width;


        private readonly float _heightScaleFactor;


        public InputResult(float heightScaleFactor)
        {
            _heightScaleFactor = heightScaleFactor;

            State = InputState.None;
            Time = new InputTime();
            Position = new InputPosition();
        }
    }


    public struct InputTime
    {
        public float Start;
        public float Now;

        public float Elapsed => Now - Start;
    }

    public struct InputPosition
    {
        public Vector3 Down;
        public Vector3 Previous;
        public Vector3 Current;
    }

    public enum InputState
    {
        None,
        Moved,
        Hold
    }
}