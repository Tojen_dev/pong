﻿using System.Collections;
using UnityEngine;

namespace Gameplay
{
    public class ShowWithAnimation : MonoBehaviour
    {
        [SerializeField]
        private Animator animator;


        public virtual void Show()
        {
            gameObject.SetActive(true);

            if (gameObject.activeInHierarchy)
                StartCoroutine(HideWaiter());
        }

        private IEnumerator HideWaiter()
        {
            yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
            Hide();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}