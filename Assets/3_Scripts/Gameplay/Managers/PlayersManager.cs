using System;
using Gameplay.Player;
using UnityEngine;

namespace Gameplay.Managers
{
    public class PlayersManager : MonoBehaviour
    {
        [SerializeField]
        private PlayerPointPair[] playersWithPoints;


        public void PlacePlayers()
        {
            for (var i = 0; i < playersWithPoints.Length; i++)
                playersWithPoints[i].player.SetPosition(playersWithPoints[i].placePoint.position);
        }

        public void ActivatePlayers()
        {
            for (var i = 0; i < playersWithPoints.Length; i++)
                playersWithPoints[i].player.Activate();
        }

        public void DeactivatePlayers()
        {
            for (var i = 0; i < playersWithPoints.Length; i++)
                playersWithPoints[i].player.Deactivate();
        }

        private void OnDisable()
        {
            DeactivatePlayers();
        }
    }

    [Serializable]
    public class PlayerPointPair
    {
        public PlayerController player;
        public Transform placePoint;
    }
}