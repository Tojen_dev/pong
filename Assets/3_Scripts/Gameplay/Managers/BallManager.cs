using DG.Tweening;
using UnityEngine;

namespace Gameplay.Managers
{
    public class BallManager : MonoBehaviour
    {
        [SerializeField, Range(0, 1)]
        private float maxRandomDirectionX = .5f;

        [SerializeField]
        private Ball ball;

        [Space]
        [SerializeField]
        private BallSettings[] settings;


        public void LaunchRandom()
        {
            Vector2 direction = new Vector2
            {
                x = Random.value * maxRandomDirectionX,
                y = Random.value > .5f ? 1 : -1
            };

            DOVirtual.DelayedCall(.5f, () => ball.Launch(direction.normalized));
        }

        private void SetupRandom()
        {
            int index = Random.Range(0, settings.Length);
            ball.Load(settings[index]);
        }


        public void Reset()
        {
            ball.Reset();
            SetupRandom();
        }
    }
}