using System;
using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(menuName = "EventChannels/GameResultEventChannel", fileName = "GameResultEventChannel")]
    public class GameResultEventChannel : ScriptableObject
    {
        public event Action Lose;
        public void CallLose() => Lose?.Invoke();
    }
}