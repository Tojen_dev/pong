using System;
using Controllers;
using Extensions;
using Gameplay.Level;
using UnityEngine;

namespace Gameplay.Player
{
    public class PlayerController : MonoBehaviour
    {
        [Header("Event channels:")]
        [SerializeField]
        private ScoreEventChannel scoreEventChannel;

        public PlayerAnimation Animation { get; private set; }
        public PlayerMovement Movement { get; private set; }

        public ScoreEventChannel ScoreEventChannel => scoreEventChannel;

        public Vector2 Position
        {
            get => transform.position;
            set => transform.position = value;
        }

        private PlayerCollisionHandler _collisionHandler;


        private void Awake()
        {
            _collisionHandler = new PlayerCollisionHandler(this);

            Animation = GetComponent<PlayerAnimation>();
            Movement = GetComponent<PlayerMovement>();
        }


        private void FixedUpdate()
        {
            Movement.Move();
        }


        public void SetPosition(Vector3 position)
        {
            Movement.SetPosition(position);
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            _collisionHandler.Handle(collision);
        }


        public void Activate()
        {
            InputHandler.OnDrag += Movement.UpdateTargetPosition;
        }

        public void Deactivate()
        {
            InputHandler.OnDrag -= Movement.UpdateTargetPosition;
        }
    }
}