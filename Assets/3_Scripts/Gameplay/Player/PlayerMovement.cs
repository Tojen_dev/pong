using Controllers;
using UnityEngine;

namespace Gameplay.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField]
        private float speed;

        [SerializeField]
        private float sensitivity;

        [SerializeField, Range(0, 3)]
        private float bound;

        private Rigidbody2D _rigidbody;

        private Vector2 _targetPosition;


        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }


        public void UpdateTargetPosition(InputResult input)
        {
            _targetPosition.x += input.DeltaFrameTransformed.x * sensitivity;
            _targetPosition.x = Mathf.Clamp(_targetPosition.x, -bound, bound);
        }

        public void Move()
        {
            if (_rigidbody.position.x == _targetPosition.x)
                return;

            Vector2 newPosition = Vector2.MoveTowards(_rigidbody.position, _targetPosition, speed * Time.deltaTime);
            _rigidbody.position = newPosition;
        }


        public void SetPosition(Vector2 position)
        {
            _targetPosition = position;
            _rigidbody.position = position;
        }
    }
}