using Extensions;
using UnityEngine;
using UnityEngine.UIElements;

namespace Gameplay.Player
{
    public class PlayerCollisionHandler
    {
        private readonly PlayerController _player;


        public PlayerCollisionHandler(PlayerController player)
        {
            _player = player;
        }


        public void Handle(Collision2D collision)
        {
            if (collision.gameObject.TryGetComponent(PickPlace.Parent, out Ball ball))
                BallCollisionHandler(collision, ball);
        }

        private void BallCollisionHandler(Collision2D collision, Ball ball)
        {
            _player.Animation.ReceiveBall();
            _player.ScoreEventChannel.CallScored(false);

            LaunchBall(collision, ball);
        }

        private void LaunchBall(Collision2D collision, Ball ball)
        {
            Vector2 collisionPosition = collision.contacts[0].point;
            Vector2 currentPosition = _player.Position;
            float boardWidth = collision.collider.bounds.size.x;
            float x = (collisionPosition.x - currentPosition.x) / boardWidth;

            Vector3 direction = new Vector3(x, -Mathf.Sign(currentPosition.y)).normalized;
            ball.Launch(direction);
        }
    }
}