namespace Gameplay.Player
{
    public class PlayerAnimation : AnimationController
    {
        private readonly int _receiveBallHash = UnityEngine.Animator.StringToHash("ReceiveBall");


        public void ReceiveBall()
        {
            SetTrigger(_receiveBallHash);
        }
    }
}