﻿using UnityEngine;

namespace Gameplay
{
    public class CompleteEffect : MonoBehaviour
    {
        [SerializeField]
        private float offsetY;


        public void Show(Transform target)
        {
            Place(target);
            gameObject.SetActive(true);
        }

        private void Place(Transform target)
        {
            transform.position = target.position + Vector3.up * offsetY;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}