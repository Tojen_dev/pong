﻿using Constants;
using Extensions;
using UnityEngine;

namespace Gameplay.Player
{
    public class AnimationController : MonoBehaviour
    {
        [SerializeField]
        private Animator animator;

        protected Animator Animator => animator;


        #region Set

        public void SetTrigger(int hash, bool resetAll = true) => animator.SetTrigger(hash, resetAll);
        public void SetFloat(int hash, float value) => animator.SetFloat(hash, value);
        public void SetBool(int hash, bool value) => animator.SetBool(hash, value);

        #endregion

        #region Play

        public void Play(int hash, int layer = 0, float normalizedTime = 0)
        {
            Animator.Play(hash, layer, normalizedTime);
        }

        public void Play(string clipName, int layer = 0, float normalizedTime = 0)
        {
            Animator.Play(clipName, layer, normalizedTime);
        }

        public void PlayRandom(int hash, int layer, float normalizedTimeDuration = .1f)
        {
            float normalizedTimeOffset = Random.value;
            Animator.CrossFade(hash, normalizedTimeDuration, layer, normalizedTimeOffset);
        }

        #endregion


        public void Reset()
        {
            animator.SetTrigger(AnimatorHash.Reset);
        }
    }
}