using Extensions;
using Gameplay.Feedbacks;
using UnityEngine;

namespace Gameplay
{
    public class Border : MonoBehaviour
    {
        [SerializeField]
        private float minRicochetDirectionX;

        [SerializeField]
        private EffectFeedback ballHitEffect;


        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.TryGetComponent(PickPlace.Parent, out Ball ball))
                BallCollisionHandler(collision, ball);
        }

        private void BallCollisionHandler(Collision2D collision, Ball ball)
        {
            ballHitEffect.Execute(collision.contacts[0].point);

            LaunchBall(collision, ball);
        }

        private void LaunchBall(Collision2D collision, Ball ball)
        {
            ballHitEffect.Execute(collision.contacts[0].point);

            Vector2 direction = Vector2.Reflect(ball.Direction, collision.contacts[0].normal).normalized;

            if (direction.x > -.25f && direction.x < .25f)
            {
                direction.x = -minRicochetDirectionX * Mathf.Sign(transform.position.x);
                direction.Normalize();
            }

            ball.Launch(direction);
        }
    }
}