using Extensions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Gameplay
{
    public class Ball : MonoBehaviour
    {
        [SerializeField]
        private float speed;

        [SerializeField]
        private SpriteRenderer renderer;

        [SerializeField]
        private TrailRenderer trail;


        private Rigidbody2D _rigidbody;


        public Vector2 Direction => _rigidbody.velocity.normalized;


        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        public void Launch(Vector2 direction)
        {
            _rigidbody.isKinematic = false;
            _rigidbody.velocity = direction * speed;
        }


        public void Load(BallSettings settings)
        {
            speed = settings.Speed;
            transform.localScale = Vector2.one * settings.Size;
        }

        public void ChangeAppearance(Sprite sprite)
        {
            renderer.sprite = sprite;
        }

        public void Reset()
        {
            transform.Reset();

            _rigidbody.isKinematic = true;
            _rigidbody.velocity = Vector2.zero;
            _rigidbody.angularVelocity = 0;

            trail.Clear();
        }
    }
}