using Gameplay.Player;
using PlayerSave;
using Shop;
using UnityEngine;

namespace Gameplay
{
    public class BallCustomizer : MonoBehaviour
    {
        [SerializeField]
        private Ball ball;

        [SerializeField]
        private PlayerData playerData;

        [Space]
        [SerializeField]
        private SpriteAppearancesList balls;


        private void Awake()
        {
            playerData.AppearanceChanged += UpdateAppearance;
        }


        private void Start()
        {
            UpdateAppearance(playerData.SelectedSkinInfo.itemIndex);
        }

        private void UpdateAppearance(int index)
        {
            Sprite appearance = balls[index];
            ball.ChangeAppearance(appearance);
        }


        private void OnDisable()
        {
            playerData.AppearanceChanged -= UpdateAppearance;
        }
    }
}