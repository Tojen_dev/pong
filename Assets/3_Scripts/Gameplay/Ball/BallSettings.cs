using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(menuName = "Gameplay/BallSettings", fileName = "BallSettings")]
    public class BallSettings : ScriptableObject
    {
        [SerializeField]
        private float speed;

        [SerializeField]
        private float  size;


        public float Speed => speed;
        public float  Size => size;
    }
}