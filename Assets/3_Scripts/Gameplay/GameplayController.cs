﻿using Gameplay.Managers;
using UnityEngine;
using Zenject;

namespace Gameplay
{
    public class GameplayController : MonoBehaviour
    {
        [Inject]
        private PlayersManager _playersManager;

        [Inject]
        private BallManager _ballManager;


        public void Activate()
        {
            _playersManager.ActivatePlayers();
            _ballManager.LaunchRandom();
        }

        public void Deactivate()
        {
            _playersManager.DeactivatePlayers();
        }


        public void Tick()
        {
        }

        public void Reset()
        {
            _playersManager.PlacePlayers();
            _ballManager.Reset();
        }
    }
}