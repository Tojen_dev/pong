using Extensions;
using UnityEngine;

namespace Gameplay
{
    public class LoseZone : MonoBehaviour
    {
        [SerializeField]
        private GameResultEventChannel gameResultEventChannel;
        
        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.gameObject.TryGetComponent(PickPlace.Parent, out Ball ball))
                gameResultEventChannel.CallLose();
        }
    }
}