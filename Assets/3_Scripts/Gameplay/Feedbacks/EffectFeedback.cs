using Pool;
using UnityEngine;

namespace Gameplay.Feedbacks
{
    public class EffectFeedback : MonoBehaviour
    {
        [SerializeField]
        private PoolEffect effect;

        [SerializeField]
        private bool originalRotation;

        [Space]
        [SerializeField]
        private Transform holder;


        public void Execute(Transform container)
        {
            PoolManager.Instance.PopOrCreate(effect, container);
        }

        public void Execute(Vector3 position)
        {
            Execute(holder, position);
        }

        public void Execute(Transform container, Vector3 position)
        {
            Quaternion rotation = originalRotation ? effect.transform.rotation : Quaternion.identity;
            PoolManager.Instance.PopOrCreate(effect, position, rotation, container);
        }
    }
}