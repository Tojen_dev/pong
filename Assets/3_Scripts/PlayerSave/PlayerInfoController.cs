﻿using System;
using Others;
using UnityEngine;

namespace PlayerSave
{
    public class PlayerInfoController : MonoBehaviour
    {
        [SerializeField]
        private PlayerData playerData;

        [SerializeField]
        private PlayerSerializedData serializedData;


        private void Awake()
        {
            serializedData = DataSaver.GetEncrypted<PlayerSerializedData>() ?? new PlayerSerializedData();
            playerData.Load(serializedData);
        }

        public void UpdateAppearance(int itemIndex)
        {
            serializedData.selectedSkinInfo.itemIndex = itemIndex;
            playerData.SelectedSkinInfo.itemIndex = itemIndex;

            Save();
            
            playerData.CallAppearanceChanged();
        }

        private void Save()
        {
            DataSaver.SaveEncrypted(serializedData);
        }
    }
}