using System;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerSave
{
    [CreateAssetMenu(menuName = "Infos/Player", fileName = "PlayerInfo")]
    public class PlayerData : ScriptableObject
    {
        public event Action<int> AppearanceChanged;
        public void CallAppearanceChanged() => AppearanceChanged?.Invoke(SelectedSkinInfo.itemIndex);


        private PlayerSerializedData _data;
        public SelectedSkinInfo SelectedSkinInfo => _data.selectedSkinInfo;


        public void Load(PlayerSerializedData data)
        {
            _data = data;
        }
    }

    [Serializable]
    public class PlayerSerializedData
    {
        public SelectedSkinInfo selectedSkinInfo;

        public PlayerSerializedData()
        {
            selectedSkinInfo = new SelectedSkinInfo();
        }
    }
}