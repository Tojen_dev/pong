using System;
using UnityEngine;

namespace Level
{
    [CreateAssetMenu(menuName = "Settings/Levels/LevelsSettings")]
    public class LevelsSettings : ScriptableObject
    {
#if UNITY_EDITOR
        [Range(1, 25)]
        public int level;
#endif

        [Header("Sets:")]
        public int skipSets;

        [Space]
        public LevelsSet[] sets;
    }
}