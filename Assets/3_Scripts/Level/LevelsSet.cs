﻿using UnityEngine;

namespace Level
{
    [CreateAssetMenu(menuName = "Settings/Levels/Set")]
    public class LevelsSet : ScriptableObject
    {
        public string name;
        public Sprite icon;

        [Space]
        public LevelInfo[] levels;
    }
}
