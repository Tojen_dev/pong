﻿using Gameplay;
using UI.Core.Panels;
using Ui.Panels;
using UnityEngine;
using Zenject;

namespace Level
{
    public class LevelsLoader : MonoBehaviour
    {
        [SerializeField]
        private LevelInfo level;

        [Inject]
        private LevelsManager _levelsManager;

        [Inject]
        private PanelsHolder _panelsHolder;

        [Inject]
        private LevelCreator _levelCreator;

        [Inject]
        private GameplayController _gameplayController;


        public LevelInfo CurrentLevel => level ? level : _levelCreator.Level;


        public void LoadLevel()
        {
            if (_panelsHolder.TryGetConcretePanel(PanelName.Shared, out SharedPanel panel))
                panel.UpdateLevel(_levelsManager.Level);

            //if (level == null && (level = FindObjectOfType<LevelInfo>()) == null)
            _levelCreator.Create(_levelsManager.Info);
            //else
            //    level.Reset();
        }
    }
}