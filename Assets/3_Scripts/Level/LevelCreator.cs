﻿using UnityEngine;

namespace Level
{
    public class LevelCreator
    {
        public LevelInfo Level { get; private set; }


        public void Create(LevelInfo info)
        {
            Clear();

            if (info)
            {
                Level = Object.Instantiate(info);
            }
        }


        private void Clear()
        {
            if (Level)
                Object.Destroy(Level.gameObject);
        }
    }
}