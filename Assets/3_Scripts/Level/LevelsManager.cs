﻿using System;
using Others;
using UnityEngine;
using Zenject;


namespace Level
{
    public class LevelsManager : MonoBehaviour, IInitializable
    {
        [SerializeField]
        private LevelsSettings settings;


        public int Level { get; private set; }

        public LevelInfo Info { get; private set; }
        public int TotalLevels { get; private set; }


        public int LevelIndex => _levelIndex;
        public LevelsSet CurrentSet => settings.sets[_setIndex];

        public LevelsSet NextSet
        {
            get
            {
                int index = _setIndex + 1;
                if (index >= settings.sets.Length)
                    index = 0;

                return settings.sets[index];
            }
        }


        private int _setIndex;
        private int _levelIndex;
        private bool _setsEnded;
        public int TotalGamesPlayed { get; private set; }
        public int LevelsLoop { get; private set; }
        public int LevelNumber { get; private set; }
        public bool IsRandomLevel => false;

        private const string LevelKey = "LevelKey";
        private const string TotalGamesCountKey = "TotalGamesCountKey";
        private const string LevelsLoopKey = "LevelsLoopKey";


        public void Initialize()
        {
#if !UNITY_EDITOR
             Level = DataSaver.GetIntProperty(LevelKey, 1);
#else
            Level = settings.level;
#endif

            LoadTotalGamesCount();
            LoadLevelsLoop();
            CalculateLevels();
            LoadLevel();
            CalculateLevelNumber();
        }

        private void LoadTotalGamesCount()
        {
            TotalGamesPlayed = DataSaver.GetIntProperty(TotalGamesCountKey);
        }

        public void IncrementTotalGamesPlayed()
        {
            TotalGamesPlayed++;
            DataSaver.Save(TotalGamesPlayed);
        }

        private void LoadLevelsLoop()
        {
            LevelsLoop = DataSaver.GetIntProperty(LevelsLoopKey);
        }

        public void IncrementLevelsLoop()
        {
            LevelsLoop++;
            DataSaver.Save(LevelsLoop);
        }

        private void CalculateLevels()
        {
            if (settings.sets != null)
            {
                for (int i = 0; i < settings.sets.Length; i++)
                {
                    TotalLevels += settings.sets[i].levels.Length;
                }
            }
        }

        private void CalculateLevelNumber()
        {
            LevelNumber = 1;

            for (int i = 0; i < _setIndex; i++)
                LevelNumber += settings.sets[i].levels.Length;

            LevelNumber += _levelIndex;
        }

        private void LoadLevel()
        {
            if (settings.sets.Length == 0 || settings.sets[0].levels.Length == 0)
                return;

            int level = Level;

            while (true)
            {
                int length = settings.sets[_setIndex].levels.Length;

                if (level > length)
                {
                    level -= length;

                    bool setsEnded = SelectNextSet();
                    if (!_setsEnded && setsEnded)
                    {
                        _setsEnded = true;
                        int skipLoops = level / TotalLevels;
                        level -= TotalLevels * skipLoops;
                    }
                }
                else
                {
                    _levelIndex = level - 1;

                    if (_levelIndex < 0)
                        _levelIndex = 0;

                    Info = CurrentSet.levels[_levelIndex];
                    break;
                }
            }
        }

        public LevelInfo GetInfo(int level)
        {
            if (settings.sets.Length == 0 || settings.sets[0].levels.Length == 0)
                return null;
            int setIndex = 0;

            while (true)
            {
                int length = settings.sets[setIndex].levels.Length;

                if (level > length)
                {
                    level -= length;
                    setIndex++;
                }
                else
                {
                    int levelIndex = level - 1;
                    Info = settings.sets[setIndex].levels[levelIndex];
                    return Info;
                }
            }
        }


        public void NextLevel()
        {
            Level++;
            DataSaver.SaveProperty(LevelKey, Level);

            LoadNextLevel();
        }

        private void LoadNextLevel()
        {
            if (TotalLevels == 0)
                return;

            _levelIndex++;

            if (_levelIndex >= CurrentSet.levels.Length)
            {
                _levelIndex = 0;
                bool ended = SelectNextSet();
                if (ended)
                    IncrementLevelsLoop();
            }

            Info = CurrentSet.levels[_levelIndex];
        }

        private bool SelectNextSet()
        {
            _setIndex++;

            if (_setIndex >= settings.sets.Length)
            {
                _setIndex = 0;
                return true;
            }

            return false;
        }


        public void PreviousLevel()
        {
            Level--;
            DataSaver.SaveProperty(LevelKey, Level);

            LoadNextLevel();
        }

        public void SetLevel(int level)
        {
            Level = level;
            DataSaver.SaveProperty(LevelKey, Level);

            LoadLevel();
        }

        private void CreateRandomLevel()
        {
            // Info = new LevelInfo {floors = new Floor[_settings.randomLevelInfo.count]};
            //
            // Floor[] floors = _settings.randomLevelInfo.floors;
            // List<int> indices = Enumerable.Range(0, floors.Length).ToList();
            //
            // Random random = new Random(Level);
            // for (int i = 0; i < Info.floors.Length; i++)
            // {
            //     int index = random.Next(0, indices.Count);
            //     int floorIndex = indices[index];
            //     indices.RemoveAt(index);
            //
            //     Info.floors[i] = floors[floorIndex];
            // }
        }


        public void Reset()
        {
        }

        public void ResetLevel()
        {
            _setIndex = 0;
            _levelIndex = 0;
            Level = 1;
            Info = CurrentSet.levels[_levelIndex];

            DataSaver.SaveProperty(LevelKey, Level);
        }
    }
}