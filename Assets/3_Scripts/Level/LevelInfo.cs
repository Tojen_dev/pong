﻿using UnityEngine;

namespace Level
{
    public class LevelInfo : MonoBehaviour
    {
        [SerializeField]
        private LevelDifficult difficult;

        [SerializeField]
        private LevelType type;


        public LevelDifficult Difficult => difficult;
        public LevelType Type => type;
    }

    public enum LevelDifficult
    {
        Easy,
        Hard,
    }

    public enum LevelType
    {
        Normal,
        Bonus,
        Boss,
    }
}