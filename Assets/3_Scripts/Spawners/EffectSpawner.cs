﻿using Pool;
using UnityEngine;

namespace Spawners
{
    public class EffectSpawner : MonoBehaviour
    {
        [SerializeField]
        private PoolEffect effect;

        [Space]
        [SerializeField]
        private Transform container;


        public void Spawn(Vector3 position, bool useOriginalRotation = true)
        {
            Quaternion rotation = useOriginalRotation ? effect.transform.rotation : Quaternion.identity;
            PoolManager.Instance.PopOrCreate(effect, position, rotation, container);
        }

        public void Spawn(Transform parent)
        {
            PoolManager.Instance.PopOrCreate(effect, parent);
        }
    }
}