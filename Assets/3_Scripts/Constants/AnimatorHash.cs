﻿using UnityEngine;

namespace Constants
{
    public static class AnimatorHash
    {
        public static readonly int Idle = Animator.StringToHash("Idle");
        public static readonly int Run = Animator.StringToHash("Run");
        public static readonly int Victory = Animator.StringToHash("Victory");
        public static readonly int Lose = Animator.StringToHash("Lose");
        public static readonly int Die = Animator.StringToHash("Die");
        public static readonly int Reset = Animator.StringToHash("Reset");
        
        public static readonly int Attack = Animator.StringToHash("Attack");
        public static readonly int Jump = Animator.StringToHash("Jump");
        public static readonly int Walk = Animator.StringToHash("Walk");
        public static readonly int Moving = Animator.StringToHash("Moving");
        public static readonly int Pursuit = Animator.StringToHash("Pursuit");
        public static readonly int MoveSpeed = Animator.StringToHash("MoveSpeed");

        public const string IdleName = "Idle";
    }
}