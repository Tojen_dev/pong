﻿using UnityEngine;

namespace Constants
{
    public static class Layer
    {
        public static readonly int Default = LayerMask.NameToLayer("Default");
        public static readonly int Player = LayerMask.NameToLayer("Player");
        public static readonly int Enemy = LayerMask.NameToLayer("Enemy");
        public static readonly int Projectile = LayerMask.NameToLayer("Projectile");

        public static readonly int DefaultMask = LayerMask.GetMask("Default");
        public static readonly int PlayerMask = LayerMask.GetMask("Player");
        public static readonly int EnemyMask = LayerMask.GetMask("Enemy");
    }
}