﻿using UnityEngine;

namespace Constants
{
    public static class MaterialHashes
    {
        public static readonly int _frontColor = Shader.PropertyToID("_FrontColor");
        public static readonly int _topColor = Shader.PropertyToID("_TopColor");
        public static readonly int _bottomColor = Shader.PropertyToID("_BottomColor");
        public static readonly int _rightColor = Shader.PropertyToID("_RightColor");
        public static readonly int _rimColor = Shader.PropertyToID("_RimColor");
        public static readonly int _alpha = Shader.PropertyToID("_Alpha");
        public static readonly int _fogColor = Shader.PropertyToID("_FogColor");
    }
}