﻿namespace Constants
{
    public static class Tag
    {
        public const string Player = "Player";
        public const string Enemy = "Enemy";
        public const string Obstacle = "Obstacle";
        public const string Ground = "Ground";
        public const string Water = "Water";
    }
}