﻿using System;

namespace Others
{
    public static class IndexNormalizer
    {
        public static int GetLoop(int index, int count)
        {
            if (index < count)
                return index;

            index %= count;
            return index;
        }

        public static int GetRandom(int index, int count)
        {
            if (index < count)
                return index;

            var random = new Random(index);
            index = random.Next(0, count);
            return index;
        }
    }
}