﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Others
{
    public static class Navigation
    {
           public static bool InView(Transform current, Transform target, float viewAngle)
        {
            Vector3 directionToTarget = target.position - current.position;
            float angle = Mathf.Acos(Vector3.Dot(current.forward, directionToTarget.normalized));

            return angle < viewAngle;
        }


        public static Transform GetNearest(List<Transform> items, Transform item)
        {
            float minSqrDistance = float.MaxValue;
            Transform nearest = null;

            for (var i = 0; i < items.Count; i++)
            {
                float sqrDistance = (items[i].position - item.position).sqrMagnitude;

                if (sqrDistance < minSqrDistance)
                {
                    minSqrDistance = sqrDistance;
                    nearest = items[i];
                }
            }

            return nearest;
        }

        public static Transform GetNearest(IList<Transform> items, Vector3 position)
        {
            float minSqrDistance = float.MaxValue;
            Transform nearest = null;

            for (var i = 0; i < items.Count; i++)
            {
                float sqrDistance = (items[i].position - position).sqrMagnitude;

                if (sqrDistance < minSqrDistance)
                {
                    minSqrDistance = sqrDistance;
                    nearest = items[i];
                }
            }

            return nearest;
        }

        public static T GetNearest<T>(T[] items, Vector3 position) where T : MonoBehaviour
        {
            float minSqrDistance = float.MaxValue;
            T nearest = null;

            for (var i = 0; i < items.Length; i++)
            {
                float sqrDistance = (items[i].transform.position - position).sqrMagnitude;

                if (sqrDistance < minSqrDistance)
                {
                    minSqrDistance = sqrDistance;
                    nearest = items[i];
                }
            }

            return nearest;
        }

        public static Collider GetNearest(Collider[] items, Vector3 position)
        {
            float minSqrDistance = float.MaxValue;
            Collider nearest = null;

            for (var i = 0; i < items.Length; i++)
            {
                float sqrDistance = (items[i].transform.position - position).sqrMagnitude;

                if (sqrDistance < minSqrDistance)
                {
                    minSqrDistance = sqrDistance;
                    nearest = items[i];
                }
            }

            return nearest;
        }
    }
}