using UnityEngine;

namespace Others.Activators
{
    public class Activator : MonoBehaviour
    {
        [SerializeField]
        private bool activateOnAwake;

        [Space]
        [SerializeField]
        private KeyCode keyToActivate;

        [SerializeField]
        private KeyCode keyToDeactivate;

        [Space]
        [SerializeField]
        private GameObject toActivate;


        private IActivatable _activatable;


        private void Awake()
        {
            _activatable = toActivate.GetComponent<IActivatable>();

            if (activateOnAwake)
                _activatable.Activate();
        }

        private void Update()
        {
            if (Input.GetKeyDown(keyToActivate))
                _activatable.Activate();

            else if (Input.GetKeyDown(keyToDeactivate))
                _activatable.Deactivate();
        }
    }
}