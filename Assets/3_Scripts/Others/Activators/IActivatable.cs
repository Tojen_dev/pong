namespace Others.Activators
{
    public interface IActivatable
    {
        public void Activate();
        public void Deactivate();
    }
}
