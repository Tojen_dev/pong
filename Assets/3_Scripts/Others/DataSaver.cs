﻿using System;
using System.Text;
using UnityEngine;

namespace Others
{
    public static class DataSaver
    {
        public static void Save<T>(T data)
        {
            string json = JsonUtility.ToJson(data);

            PlayerPrefs.SetString(typeof(T).Name, json);
            PlayerPrefs.Save();
        }

        public static void SaveEncrypted<T>(T data)
        {
            string json = JsonUtility.ToJson(data);
            string encrypted = Encrypt(json);

            PlayerPrefs.SetString(typeof(T).Name, encrypted);
            PlayerPrefs.Save();
        }


        public static T Get<T>(T defaultValue = null) where T : class
        {
            string data = PlayerPrefs.GetString(typeof(T).Name);

            if (string.IsNullOrEmpty(data))
                return defaultValue;

            T result = JsonUtility.FromJson<T>(Decrypt(data));
            return result;
        }

        public static T GetEncrypted<T>(T defaultValue = null) where T : class
        {
            string data = PlayerPrefs.GetString(typeof(T).Name);

            if (string.IsNullOrEmpty(data))
                return defaultValue;

            T result = JsonUtility.FromJson<T>(Decrypt(data));
            return result;
        }


        public static void SaveProperty<T>(string key, T value)
        {
            PlayerPrefs.SetString(key, value.ToString());
            PlayerPrefs.Save();
        } 
        
        public static void SavePropertyEncrypted<T>(string key, T value)
        {
            string encrypted = Encrypt(value.ToString());
            PlayerPrefs.SetString(key, encrypted);
            PlayerPrefs.Save();
        }

        
        public static int GetIntProperty(string key, int defaultValue = 0)
        {
            string data = PlayerPrefs.GetString(key);

            if (string.IsNullOrEmpty(data))
                return defaultValue;

            try
            {
                return int.Parse(data);
            }
            catch (Exception e)
            {
                return defaultValue;
            }
        } 
        
        public static int GetIntPropertyEncrypted(string key, int defaultValue = 0)
        {
            string data = PlayerPrefs.GetString(key);

            if (string.IsNullOrEmpty(data))
                return defaultValue;

            try
            {
                string result = Decrypt(data);
                return int.Parse(result);
            }
            catch (Exception e)
            {
                return defaultValue;
            }
        }

        
        public static void Clear(Type type)
        {
            PlayerPrefs.SetString(type.Name, string.Empty);
            PlayerPrefs.Save();
        }


        private static string Encrypt(string toEncrypt)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(toEncrypt);
            var result = BitConverter.ToString(bytes).Replace("-", "");
            return result;
        }

        private static string Decrypt(string toDecrypt)
        {
            if (!string.IsNullOrEmpty(toDecrypt))
            {
                try
                {
                    int charsCount = toDecrypt.Length;
                    byte[] bytes = new byte[charsCount / 2];

                    for (int i = 0; i < charsCount; i += 2)
                    {
                        bytes[i / 2] = Convert.ToByte(toDecrypt.Substring(i, 2), 16);
                    }

                    var result = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                    return result;
                }
                catch (Exception e)
                {
                    return null;
                }
            }

            return null;
        }
    }
}