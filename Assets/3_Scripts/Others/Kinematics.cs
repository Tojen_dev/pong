﻿using UnityEngine;

namespace Others
{
    public static class Kinematics
    {
        public static LaunchData CalculateLaunchData(Vector3 fromPosition, Vector3 targetPosition, float height)
        {
            float gravity = Physics.gravity.y;
            Vector3 displacement = targetPosition - fromPosition;

            float time = Mathf.Sqrt(-2 * height / gravity) + Mathf.Sqrt(2 * (displacement.y - height) / gravity);

            Vector3 velocity = displacement / time;
            velocity.y = -Mathf.Sign(gravity) * Mathf.Sqrt(-2 * gravity * height);

            return new LaunchData(velocity, time);
        }


        public static Vector3[] GetPositions(Vector3 from, Vector3 force, int resolution, float time)
        {
            Vector3[] positions = new Vector3[resolution];

            for (int i = 0; i < resolution; i++)
            {
                float simulationTime = i / (float) resolution * time;

                Vector3 displacement = force * simulationTime +
                                       Vector3.up * (Physics.gravity.y * Mathf.Pow(simulationTime, 2)) / 2f;
                Vector3 position = from + displacement;
                positions[i] = position;
            }

            return positions;
        }

        public static float GetTimeByFinalVelocity(float initVelocityY, float finalVelocityY = 0)
        {
            //Vf = V0 + a * t
            float time = (finalVelocityY - initVelocityY) / Physics.gravity.y;
            return time;
        }

        public static float GetHeightByFinalVelocity(float initPositionY, float initVelocityY, float finalVelocityY = 0)
        {
            //Vf^2 = V0^2 + 2 * a * d 
            //d is a peek height
            float height = initPositionY;
            height += (finalVelocityY * finalVelocityY - initVelocityY * initVelocityY) /
                      (2 * Physics.gravity.y);

            return height;
        }
    }

    public struct LaunchData
    {
        public readonly Vector3 InitialVelocity;
        public readonly float TimeToTarget;

        public LaunchData(Vector3 initialVelocity, float timeToTarget)
        {
            InitialVelocity = initialVelocity;
            TimeToTarget = timeToTarget;
        }
    }
}