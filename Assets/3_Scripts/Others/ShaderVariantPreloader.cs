using UnityEngine;

namespace Preloaders
{
    public class ShaderVariantPreloader : MonoBehaviour
    {
        [SerializeField]
        private ShaderVariantCollection[] collections;

        
        private void Awake()
        {
            WarmupShaderVariantCollection();
        }

        private void WarmupShaderVariantCollection()
        {
            if (collections != null)
            {
                for (var i = 0; i < collections.Length; i++)
                {
                    collections[i].WarmUp();
                }
            }
        }
    }
}