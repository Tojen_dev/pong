﻿using System;
using System.Collections;
using UnityEngine;

namespace Others
{
    public class CoroutineRunner : MonoBehaviour
    {
        public static CoroutineRunner Instance;

        private void Awake()
        {
            Instance = this;
        }


        public IEnumerator Wait(float delay, Action action)
        {
            return Run(WaitResult(delay, action));
        }

        private IEnumerator WaitResult(float delay, Action action)
        {
            yield return new WaitForSeconds(delay);
            action?.Invoke();
        }


        public IEnumerator Run(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
            return coroutine;
        }

        public void Stop(IEnumerator coroutine)
        {
            if (coroutine != null)
                StopCoroutine(coroutine);
        }
    }
}