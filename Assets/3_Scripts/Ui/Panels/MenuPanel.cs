﻿using UI.Core.Panels;
using UnityEngine;

namespace Ui.Panels
{
    public class MenuPanel : Panel
    {
        [SerializeField]
        private GameObject bestScoreContainer;


        public override void Show()
        {
            base.Show();
            bestScoreContainer.SetActive(true);
        }

        public override void Hide()
        {
            base.Hide();
            bestScoreContainer.SetActive(false);
        }
    }
}