using System.Collections.Generic;
using Extensions;
using UI.Core.Panels;
using UnityEngine;
using Zenject;

namespace Ui.Panels
{
    public class PanelsHolder : MonoBehaviour, IInitializable
    {
        [Inject]
        private Panel[] _panels;

        private Dictionary<PanelName, Panel> _panelsDictionary;


        #region Initialization

        public void Initialize()
        {
            FillDictionary();
        }

        private void FillDictionary()
        {
            _panelsDictionary = new Dictionary<PanelName, Panel>();

            for (var i = 0; i < _panels.Length; i++)
                _panelsDictionary.Add(_panels[i].Name, _panels[i]);
        }

        #endregion

        #region Getting panel

        public Panel GetPanel(PanelName panelName)
        {
            _panelsDictionary.TryGetValue(panelName, out Panel panel);
            return panel;
        }

        public T GetConcretePanel<T>(PanelName panelName) where T : Panel
        {
            _panelsDictionary.TryGetValue(panelName, out Panel panel);
            return panel as T;
        }

        public T GetConcretePanel<T>(T panelType) where T : Panel
        {
            for (var i = 0; i < _panels.Length; i++)
            {
                if (_panels[i] is T)
                    return _panels[i] as T;
            }

            return null;
        }

        public bool TryGetPanel(PanelName panelName, Panel result)
        {
            _panelsDictionary.TryGetValue(panelName, out Panel panel);
            return panel;
        }

        public bool TryGetConcretePanel<T>(PanelName panelName, out T result) where T : Panel
        {
            _panelsDictionary.TryGetValue(panelName, out Panel panel);
            result = panel as T;

            return result != null;
        }

        public bool TryGetConcretePanel<T>(out T result) where T : Panel
        {
            for (var i = 0; i < _panels.Length; i++)
            {
                if (_panels[i] is T value)
                {
                    result = value;
                    return true;
                }
            }

            result = null;
            return false;
        }

        #endregion

        #region Switching

        /// <summary>
        /// Hide all panels, except names
        /// </summary>
        /// <param name="names">Panel names to show</param>
        public void SwitchPanels(params PanelName[] names)
        {
            HideAll();
            ShowPanels(names);
        }

        public void ShowPanels(params PanelName[] names)
        {
            for (int i = 0; i < names.Length; i++)
            {
                if (_panelsDictionary.TryGetValue(names[i], out Panel panel))
                    panel.Show();
            }
        }

        public void HidePanels(params PanelName[] names)
        {
            for (int i = 0; i < names.Length; i++)
            {
                if (_panelsDictionary.TryGetValue(names[i], out Panel panel))
                    panel.Hide();
            }
        }

        public void HideAll()
        {
            for (var i = 0; i < _panels.Length; i++)
            {
                _panels[i].Hide();
            }
        }

        #endregion
    }
}