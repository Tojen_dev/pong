﻿using Packages.UI.Core;
using TMPro;
using UI.Core.Panels;
using UnityEngine;

namespace Ui.Panels
{
    public class SharedPanel : Panel
    {
        [SerializeField]
        private TextMeshProUGUI levelText;


        public void UpdateLevel(int level)
        {
            levelText.text = "LEVEL " + level;
        }
    }
}