﻿using System;
using System.Collections;
using System.Collections.Generic;
using Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsUi : MonoBehaviour
{
    [SerializeField]
    private ButtonInfo _soundButtonInfo;

    [SerializeField]
    private ButtonInfo _vibrationButtonInfo;


    private Animator _animator;

    private bool _isActive;
    private bool _isMoving;

    public AnimationClip openClip;
    public AnimationClip closeClip;


    private const string animBaseLayer = "Base Layer";
    private int openSettingsHash = Animator.StringToHash(animBaseLayer + ".OpenSettings");
    private int closeSettingsHash = Animator.StringToHash(animBaseLayer + ".CloseSettings");

    private Dictionary<int, AnimationClip> hashToClip = new Dictionary<int, AnimationClip>();


    public void Initialize()
    {
//        AudioController.OnLoad += OnAudioLoad;
//        AudioController.OnSoundUpdated += UpdateSound;
//        AudioController.OnTapticUpdated += UpdateTaptic;

        _animator = GetComponent<Animator>();

        hashToClip.Add(openSettingsHash, openClip);
        hashToClip.Add(closeSettingsHash, closeClip);
    }

//    private void OnAudioLoad(AudioController.AudioInfo info)
//    {
//        UpdateSound(info.SoundOn);
//        UpdateTaptic(info.TapticOn);
//    }

    public void SettingsClick()
    {
        _isActive = !_isActive;

        float time;
        var stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
        if (stateInfo.fullPathHash == openSettingsHash || stateInfo.fullPathHash == closeSettingsHash)
        {
            time = GetCurrentAnimatorTime(_animator, 0);
            time = Mathf.Clamp01(1 - time);
        }
        else
        {
            time = _isActive ? 0 : 1;
        }

        _animator.Play(_isActive ? openSettingsHash : closeSettingsHash, 0, time);
    }


    public void ToGame()
    {
        ActivateButtons(false);
    }

    public void ToMenu()
    {
        _isActive = false;

        _vibrationButtonInfo.Image.gameObject.SetActive(false);
        _soundButtonInfo.Image.gameObject.SetActive(false);
    }

    private void ActivateButtons(bool activate)
    {
       // _soundButtonInfo.Button.interactable = activate;
      //  _vibrationButtonInfo.Button.interactable = activate;
    }


    private void UpdateTaptic(bool active)
    {
        _vibrationButtonInfo.Image.sprite = active ? _vibrationButtonInfo.Icons.On : _vibrationButtonInfo.Icons.Off;
    }

    private void UpdateSound(bool active)
    {
        _soundButtonInfo.Image.sprite = active ? _soundButtonInfo.Icons.On : _soundButtonInfo.Icons.Off;
    }


    AnimationClip GetClipFromHash(int hash)
    {
        AnimationClip clip;
        if (hashToClip.TryGetValue(hash, out clip))
            return clip;
        else
            return null;
    }

    public float GetCurrentAnimatorTime(Animator targetAnim, int layer = 0)
    {
        AnimatorStateInfo animState = targetAnim.GetCurrentAnimatorStateInfo(layer);
        //Get the current animation hash
        int currentAnimHash = animState.fullPathHash;

        //Convert the animation hash to animation clip
        AnimationClip clip = GetClipFromHash(currentAnimHash);

        //Get the current time
        float currentTime = clip.length * animState.normalizedTime;
        return currentTime;
    }


    #region Inner

    [Serializable]
    public struct ButtonInfo
    {
        public Image Image;
        //public Button Button;
        public OnOffIcons Icons;
    }

    [Serializable]
    public struct OnOffIcons
    {
        public Sprite On;
        public Sprite Off;
    }

    #endregion
}