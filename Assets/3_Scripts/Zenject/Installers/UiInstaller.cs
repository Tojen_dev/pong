﻿using UI.Core.Panels;
using Ui.Panels;
using UnityEngine;

namespace Zenject.Installers
{
    public class UiInstaller : MonoInstaller
    {
        [SerializeField]
        private PanelsHolder panelsHolder;

        [Space]
        [SerializeField]
        private Panel[] panels;


        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<Panel[]>().FromInstance(panels).AsSingle();
            Container.BindInterfacesAndSelfTo<PanelsHolder>().FromInstance(panelsHolder).AsSingle();
        }
    }
}