﻿using Gameplay;
using Gameplay.Managers;
using UnityEngine;

namespace Zenject.Installers
{
    public class GameplayInstaller : MonoInstaller
    {
        [SerializeField]
        private GameplayController gameplayController;

        [SerializeField]
        private PlayersManager playersManager;

        [SerializeField]
        private BallManager ballManager;


        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<GameplayController>().FromInstance(gameplayController).AsSingle();
            Container.Bind<PlayersManager>().FromInstance(playersManager).AsSingle();
            Container.Bind<BallManager>().FromInstance(ballManager).AsSingle();
        }
    }
}