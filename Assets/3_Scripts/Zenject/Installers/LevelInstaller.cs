using Level;
using UnityEngine;

namespace Zenject.Installers
{
    public class LevelInstaller : MonoInstaller
    {
        [SerializeField]
        private LevelsLoader levelsLoader;

        [SerializeField]
        private LevelsManager levelsManager;


        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<LevelsManager>().FromInstance(levelsManager).AsSingle();
            Container.BindInterfacesAndSelfTo<LevelsLoader>().FromInstance(levelsLoader).AsSingle();
            Container.BindInterfacesAndSelfTo<LevelCreator>().AsSingle();
        }
    }
}