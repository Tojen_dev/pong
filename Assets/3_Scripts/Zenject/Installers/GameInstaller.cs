﻿using Packages.Internal.ScoreSystem.Scripts;
using States;
using UnityEngine;

namespace Zenject.Installers
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField]
        private GameStatesManager gameStatesManager;

        [SerializeField]
        private ScoreManager scoreManager;


        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<GameStatesManager>().FromInstance(gameStatesManager).AsSingle();
            Container.BindInterfacesAndSelfTo<ScoreManager>().FromInstance(scoreManager).AsSingle();
        }
    }
}