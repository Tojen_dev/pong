﻿using Controllers;
using Level;
using UnityEngine;

namespace Zenject.Settings
{
    [CreateAssetMenu(menuName = "Zenject/GameSettings")]
    public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
    {
        public override void InstallBindings()
        {
        }
    }
}