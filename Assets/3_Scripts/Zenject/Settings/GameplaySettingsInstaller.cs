﻿using UnityEngine;

namespace Zenject.Settings
{
    [CreateAssetMenu(menuName = "Zenject/GameplaySettings")]
    public class GameplaySettingsInstaller : ScriptableObjectInstaller<GameplaySettingsInstaller>
    {
        public override void InstallBindings()
        {
         
        }
    }
}