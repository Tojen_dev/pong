﻿using DG.Tweening;
using Gameplay;
using Level;
using UI.Core.Panels;
using Ui.Panels;
using Zenject;
using static States.State;

namespace States
{
    public class VictoryState : AState
    {
        [Inject]
        private LevelsManager _levelsManager;

        [Inject]
        private LevelsLoader _levelsLoader;

        [Inject]
        private PanelsHolder _panelsHolder;

        [Inject]
        private GameplayController _gameplayController;


        public override void Enter(AState from)
        {
            _levelsManager.NextLevel();
            DOVirtual.DelayedCall(1, Show);
        }

        public override void Exit(AState to)
        {
            _panelsHolder.HidePanels(PanelName.Victory);
            _levelsLoader.LoadLevel();
        }


        private void Show()
        {
            _panelsHolder.ShowPanels(PanelName.Victory);
        }


        public override State Name => Victory;
    }
}