﻿using UnityEngine;

namespace States
{
    public enum State
    {
        Menu,
        Game,
        GameOver,
        CompleteStep,
        Victory,
        StartGameTransition,
    }

    public abstract class AState : MonoBehaviour
    {
        protected GameStatesManager Manager;

        public abstract void Enter(AState from);
        public abstract void Exit(AState to);

        public virtual void Tick()
        {
        }

        public virtual void FixedTick()
        {
        }

        public virtual void LateTick()
        {
        }

        public abstract State Name { get; }

        public virtual void Initialize(GameStatesManager manager)
        {
            Manager = manager;
        }
    }
}