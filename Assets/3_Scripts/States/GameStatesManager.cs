﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;
using static States.State;

namespace States
{
    public class GameStatesManager : MonoBehaviour, IInitializable
    {
        [ReadOnly]
        public string currentState;

        public static GameStatesManager Instance { get; private set; }
        public AState TopState => _stateStack.Count == 0 ? null : _stateStack[_stateStack.Count - 1];

        private readonly List<AState> _stateStack = new List<AState>();
        private readonly Dictionary<State, AState> _statesDictionary = new Dictionary<State, AState>();


        public void Initialize()
        {
            Instance = this;

            AState[] states = GetComponents<AState>();
            if (states.Length == 0)
                return;

            //Initialize and fill dictionary
            for (int i = 0; i < states.Length; ++i)
            {
                states[i].Initialize(this);
                _statesDictionary.Add(states[i].Name, states[i]);
            }
        }

        #region Unity Events

        private void Start()
        {
            PushState(Menu);
        }

        private void Update()
        {
            if (_stateStack.Count > 0)
                _stateStack[_stateStack.Count - 1].Tick();
        }

        private void FixedUpdate()
        {
            if (_stateStack.Count > 0)
                _stateStack[_stateStack.Count - 1].FixedTick();
        }

        private void LateUpdate()
        {
            if (_stateStack.Count > 0)
                _stateStack[_stateStack.Count - 1].FixedTick();
        }

        #endregion

        #region Change States

        public void StartGame() => SwitchState(Game);
        public void ToMenu() => SwitchState(Menu);
        public void Lose() => SwitchState(GameOver);
        public void Win() => SwitchState(Victory);

        #endregion

        #region State Management

        public void SwitchState(State newState)
        {
            AState state = FindState(newState);
            if (state == null)
            {
                Debug.LogError("Can't find the state named " + newState);
                return;
            }

            _stateStack[_stateStack.Count - 1].Exit(state);
            state.Enter(_stateStack[_stateStack.Count - 1]);
            _stateStack.RemoveAt(_stateStack.Count - 1);
            _stateStack.Add(state);

            currentState = state.Name.ToString();
        }

        public AState FindState(State stateName)
        {
            AState state;
            if (!_statesDictionary.TryGetValue(stateName, out state))
            {
                return null;
            }

            return state;
        }

        public void PopState()
        {
            if (_stateStack.Count < 2)
            {
                Debug.LogError("Can't pop states, only one in stack.");
                return;
            }

            _stateStack[_stateStack.Count - 1].Exit(_stateStack[_stateStack.Count - 2]);
            _stateStack[_stateStack.Count - 2].Enter(_stateStack[_stateStack.Count - 2]);
            _stateStack.RemoveAt(_stateStack.Count - 1);
        }

        public void PushState(State stateName)
        {
            AState state;
            if (!_statesDictionary.TryGetValue(stateName, out state))
            {
                Debug.LogError("Can't find the state named " + stateName);
                return;
            }

            if (_stateStack.Count > 0)
            {
                _stateStack[_stateStack.Count - 1].Exit(state);
                state.Enter(_stateStack[_stateStack.Count - 1]);
            }
            else
            {
                state.Enter(null);
            }

            _stateStack.Add(state);
        }

        #endregion
    }
}