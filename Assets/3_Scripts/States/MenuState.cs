﻿using Gameplay;
using Level;
using UI.Core.Panels;
using Ui.Panels;
using UnityEngine;
using Zenject;
using static States.State;

namespace States
{
    public class MenuState : AState
    {
        [Inject]
        private PanelsHolder _panelsHolder;

        [Inject]
        private GameplayController _gameplayController;


        public override void Initialize(GameStatesManager manager)
        {
            base.Initialize(manager);
            Application.targetFrameRate = 60;
        }

        public override void Enter(AState from)
        {
            _panelsHolder.SwitchPanels(PanelName.Menu);
            _gameplayController.Reset();
        }

        public override void Exit(AState to)
        {
            _panelsHolder.HidePanels(PanelName.Menu);
        }


        public override State Name => Menu;
    }
}