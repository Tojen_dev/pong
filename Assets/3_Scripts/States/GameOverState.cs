﻿using DG.Tweening;
using Gameplay;
using Level;
using Packages.Internal.ScoreSystem.Scripts;
using UI.Core.Panels;
using Ui.Panels;
using Zenject;
using static States.State;

namespace States
{
    public class GameOverState : AState
    {
        [Inject]
        private LevelsManager _levelsManager;

        [Inject]
        private LevelsLoader _levelsLoader;

        [Inject]
        private PanelsHolder _panelsHolder;

        [Inject]
        private GameplayController _gameplayController;

        [Inject]
        private ScoreManager _scoreManager;
        

        public override void Enter(AState from)
        {
            _scoreManager.TrySaveBestScore();
            Show();
        }

        public override void Exit(AState to)
        {
            _panelsHolder.HidePanels(PanelName.GameOver);

            _levelsManager.Reset();
            _levelsLoader.LoadLevel();
        }


        private void Show()
        {
            _panelsHolder.ShowPanels(PanelName.GameOver);
        }


        public override State Name => GameOver;
    }
}