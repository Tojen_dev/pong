﻿using Gameplay;
using Level;
using Packages.Internal.ScoreSystem.Scripts;
using UI.Core.Panels;
using Ui.Panels;
using UnityEngine;
using Zenject;
using static States.State;

namespace States
{
    public class GameState : AState
    {
        [SerializeField]
        private GameResultEventChannel gameResultEventChannel;

        [Inject]
        private PanelsHolder _panelsHolder;

        [Inject]
        private GameplayController _gameplayController;

        [Inject]
        private ScoreManager _scoreManager;


        public override void Enter(AState from)
        {
            Subscribe();

            _scoreManager.Reset();
            _gameplayController.Activate();
            _panelsHolder.ShowPanels(PanelName.Game);
        }

        public override void Exit(AState to)
        {
            Unsubscribe();

            _gameplayController.Deactivate();
            _panelsHolder.HidePanels(PanelName.Game);
        }


        public override void Tick()
        {
            _gameplayController.Tick();
        }

        public override void FixedTick()
        {
        }



        #region Subscriptions

        private void Subscribe()
        {
            gameResultEventChannel.Lose += Manager.Lose;
        }

        private void Unsubscribe()
        {
            gameResultEventChannel.Lose -= Manager.Lose;
        }

        #endregion

        private void OnDisable()
        {
            Unsubscribe();
        }

        public override State Name => Game;
    }
}