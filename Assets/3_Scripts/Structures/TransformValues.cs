﻿using UnityEngine;

namespace Structures
{
    public class TransformValues
    {
        public readonly Vector3 Position;
        public readonly Quaternion Rotation;

        
        public TransformValues(Vector3 position, Quaternion rotation)
        {
            Position = position;
            Rotation = rotation;
        }

        public TransformValues(Vector3 position, Vector3 rotation)
        {
            Position = position;
            Rotation = Quaternion.Euler(rotation);
        }
    }
}