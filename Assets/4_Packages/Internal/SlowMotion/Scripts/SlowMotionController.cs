﻿using DG.Tweening;
using UnityEngine;

namespace Packages.SlowMotion.Scripts
{
    public class SlowMotionController : MonoBehaviour
    {
        [SerializeField]
        private SlowMotionAnimation enterSlowAnimation;

        [SerializeField]
        private SlowMotionAnimation exitSlowAnimation;


        private Sequence _animation;

        public const float DefaultFixedDeltaTime = .02f;


        public void Activate()
        {
            _animation = enterSlowAnimation.Play();
        }

        public void Deactivate()
        {
            _animation?.Kill();
            _animation = exitSlowAnimation.Play();
        }

        public void Reset()
        {
            _animation?.Kill();

            Time.timeScale = 1;
            Time.fixedDeltaTime = DefaultFixedDeltaTime;
        }
    }
}