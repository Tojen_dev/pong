﻿using DG.Tweening;
using UnityEngine;

namespace Packages.SlowMotion.Scripts
{
    [CreateAssetMenu(menuName = "Animation/Gameplay/SlowMotion")]
    public class SlowMotionAnimation : ScriptableObject
    {
        public float targetValue;
        public float duration;
        public Ease ease;

        [Space]
        [SerializeField]
        private bool useFixedDeltaTime;


        public Sequence Play()
        {
            Sequence sequence = DOTween.Sequence();

            Tween timeScale = DOTween.To(() => Time.timeScale, x => Time.timeScale = x, targetValue, duration)
                .SetEase(ease)
                .SetUpdate(true);

            sequence.Append(timeScale);

            if (useFixedDeltaTime)
            {
                Tween fixedDeltaTime = DOTween.To(() => Time.fixedDeltaTime,
                        x => Time.fixedDeltaTime = x * SlowMotionController.DefaultFixedDeltaTime, targetValue,
                        duration)
                    .SetEase(ease)
                    .SetUpdate(true);

                sequence.Join(fixedDeltaTime);
            }

            return sequence;
        }
    }
}