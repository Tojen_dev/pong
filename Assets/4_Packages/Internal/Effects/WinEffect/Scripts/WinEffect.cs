﻿using System;
using System.Collections;
using UnityEngine;

namespace Gameplay
{
    public class WinEffect : MonoBehaviour
    {
        [SerializeField]
        private float delay;

        [SerializeField]
        private int cycles;

        [SerializeField]
        private float cyclesDelay;

        [Space]
        [SerializeField]
        private ParticleSystem[] effects;

        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                Activate();

            else if (Input.GetKeyDown(KeyCode.Escape))
                Deactivate();
        }

        public void Activate()
        {
            StartCoroutine(ActivationWaiter());
        }

        public void Deactivate()
        {
            StopAllCoroutines();

            for (var i = 0; i < effects.Length; i++)
                effects[i].gameObject.SetActive(false);
        }

        IEnumerator ActivationWaiter()
        {
            var wait = new WaitForSeconds(delay);

            for (int j = 0; j < cycles; j++)
            {
                for (int i = 0; i < effects.Length; i++)
                {
                    effects[i].gameObject.SetActive(true);
                    effects[i].Play(true);

                    yield return wait;
                }
                
                yield return new WaitForSeconds(cyclesDelay);
            }
        }
    }
}