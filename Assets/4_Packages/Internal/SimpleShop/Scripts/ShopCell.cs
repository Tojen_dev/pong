﻿using System;
using Pool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Packages.Shop.Scripts.Cells
{
    public class ShopCell : PoolObject
    {
        public event Action<ShopCell> OnSelected;

        [SerializeField]
        private GameObject lockedContainer;

        [SerializeField]
        private GameObject unlockedContainer;

        [Space]
        [SerializeField]
        private Image iconImage;


        [SerializeField]
        private GameObject selectedBorder;


        public int Id { get; private set; }


        public void Initialize(int id, Sprite icon, bool unlocked)
        {
            Id = id;
            iconImage.sprite = icon;
            iconImage.SetNativeSize();

            UpdateUnlocked(unlocked);
            Deactivate();
        }

        public void UpdateUnlocked(bool unlocked)
        {
            lockedContainer.SetActive(!unlocked);
            unlockedContainer.SetActive(unlocked);
        }


        public void Select()
        {
            OnSelected?.Invoke(this);
        }

        public void Activate()
        {
            selectedBorder.SetActive(true);
        }

        public void Deactivate()
        {
            selectedBorder.SetActive(false);
        }


        public void SetIcon(Sprite iconSprite)
        {
            iconImage.sprite = iconSprite;
        }
    }
}