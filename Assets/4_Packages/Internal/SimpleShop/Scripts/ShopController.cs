using PlayerSave;
using UI.Core.Panels;
using Ui.Panels;
using UnityEngine;
using Zenject;

namespace Shop
{
    public class ShopController : MonoBehaviour
    {
        [Inject]
        private PanelsHolder _panelsHolder;

        [SerializeField]
        private ShopView ballsView;

        [Space]
        [SerializeField]
        private SpriteAppearancesList ballsList;

        [Space]
        [SerializeField]
        private PlayerData playerData;

        [SerializeField]
        private PlayerInfoController playerInfoController;


        private void Awake()
        {
            ballsView.ItemSelected += TrySelect;
        }

        public void ShowView()
        {
            _panelsHolder.SwitchPanels(PanelName.Shop);

            if (ballsView.IsLoaded == false)
            {
                int selectedIndex = playerData.SelectedSkinInfo.itemIndex;
                ballsView.LoadCells(ballsList, selectedIndex);
            }
        }


        private bool TrySelect(int cellId)
        {
            playerInfoController.UpdateAppearance(cellId);
            return true;
        }

        public void ToMenu()
        {
            _panelsHolder.SwitchPanels(PanelName.Menu);
        }

        private void OnDisable()
        {
            ballsView.ItemSelected -= TrySelect;
        }
    }
}