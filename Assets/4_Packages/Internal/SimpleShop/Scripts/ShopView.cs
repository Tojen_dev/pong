using System;
using Packages.Shop.Scripts.Cells;
using Pool;
using UI.Core.Panels;
using UnityEngine;

namespace Shop
{
    public class ShopView : Panel
    {
        public event Func<int, bool> ItemSelected;

        [SerializeField]
        private ShopCell cellPrefab;

        [SerializeField]
        private Transform cellsContainer;

        private ShopCell _selected;
        private ShopCell[] _cells;

        public bool IsLoaded { get; set; }


        private void OnEnable()
        {
            if (_cells == null)
                return;

            for (var i = 0; i < _cells.Length; i++)
            {
                _cells[i].OnSelected += TrySelectCell;
            }
        }


        public void LoadCells(SpriteAppearancesList balls, int selectedIndex)
        {
            IsLoaded = true;
            _cells = new ShopCell[balls.Count];

            for (var i = 0; i < balls.Count; i++)
            {
                _cells[i] = PoolManager.Instance.PopOrCreate(cellPrefab, cellsContainer);
                _cells[i].OnSelected += TrySelectCell;
                _cells[i].Initialize(i, balls[i], true);
            }

            SelectCell(selectedIndex);
        }

        private void TrySelectCell(ShopCell cell)
        {
            bool canSelect = ItemSelected != null && ItemSelected.Invoke(cell.Id);
            if (canSelect)
                SelectCell(cell.Id);
        }

        private void SelectCell(int index)
        {
            if (_selected)
                _selected.Deactivate();

            _selected = _cells[index];
            _selected.Activate();
            _selected.UpdateUnlocked(true);
        }

        private void OnDisable()
        {
            for (var i = 0; i < _cells.Length; i++)
            {
                _cells[i].OnSelected -= TrySelectCell;
            }
        }
    }
}