using UnityEngine;

namespace Shop
{
    [CreateAssetMenu(menuName = "Lists/SpriteAppearances", fileName = "SpriteAppearancesList")]
    public class SpriteAppearancesList : ScriptableObject
    {
        [SerializeField]
        private Sprite[] appearances;


        public int Count => appearances.Length;
        public Sprite this[int index] => appearances[index];
    }
}