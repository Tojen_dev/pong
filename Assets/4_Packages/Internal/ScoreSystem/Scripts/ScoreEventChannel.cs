using System;
using UnityEngine;

namespace Gameplay.Level
{
    [CreateAssetMenu(menuName = "EventChannels/ScoreEventChannel")]
    public class ScoreEventChannel : ScriptableObject
    {
        public event Action<bool> Scored;
        public void CallScored(bool clear) => Scored?.Invoke(clear);


        public event Action<int> ScoreUpdated;
        public void CallScoreUpdated(int score, int by) => ScoreUpdated?.Invoke(score);

        public event Action ScoreReset;
        public void CallScoreReset() => ScoreReset?.Invoke();


        public event Action<int, bool> BestScoreUpdated;
        public void CallBestScoreUpdated(int bestScore, bool loaded) => BestScoreUpdated?.Invoke(bestScore,loaded);


        public event Action<int> MultiplierUpdated;
        public void CallMultiplierIncreased(int multiplier) => MultiplierUpdated?.Invoke(multiplier);

        public event Action MultiplierReset;
        public void CallMultiplierReset(int multiplier) => MultiplierReset?.Invoke();
    }
}