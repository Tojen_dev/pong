﻿using Gameplay.Level;
using Others;
using UnityEngine;

namespace Packages.Internal.ScoreSystem.Scripts
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField]
        private ScoreEventChannel _scoreEventChannel;

        public int Score { get; private set; }
        public int BestScore { get; private set; }
        public int Multiplier { get; private set; }


        private readonly string bestScoreKey = "BestScoreKey";


        public void Awake()
        {
            BestScore = DataSaver.GetIntProperty(bestScoreKey);
            Multiplier = 1;
        }

        private void OnEnable()
        {
            _scoreEventChannel.Scored += Add;
        }

        private void Start()
        {
            _scoreEventChannel.CallBestScoreUpdated(BestScore, true);
            _scoreEventChannel.CallScoreReset();
        }


        public void Add(int value) => Add(value, false);
        public void Add(bool isPerfect) => Add(1, isPerfect);

        public int Add(int value, bool isPerfect)
        {
            int toAdd = value;
            if (isPerfect)
            {
                Multiplier++;
                toAdd *= Multiplier;

                _scoreEventChannel.CallMultiplierIncreased(Multiplier);
            }
            else
            {
                Multiplier = 1;
            }

            Score += toAdd;
            _scoreEventChannel.CallScoreUpdated(Score, toAdd);

            return toAdd;
        }

        public void TrySaveBestScore()
        {
            if (BestScore < Score)
            {
                BestScore = Score;
                DataSaver.SaveProperty(bestScoreKey, BestScore);

                _scoreEventChannel.CallBestScoreUpdated(BestScore, false);
            }
        }

        public void Reset()
        {
            Score = 0;
            Multiplier = 1;

            _scoreEventChannel.CallScoreReset();
        }


        private void OnDisable()
        {
            _scoreEventChannel.Scored -= Add;
        }
    }
}