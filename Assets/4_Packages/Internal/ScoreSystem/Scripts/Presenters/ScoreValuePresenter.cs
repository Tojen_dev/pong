using Gameplay.Level;
using Packages.UI.Core.Count.Scripts;
using UnityEngine;

namespace Packages.Internal.ScoreSystem.Scripts.Presenters
{
    public class ScoreValuePresenter : MonoBehaviour
    {
        [SerializeField]
        private ScoreEventChannel scoreEventChannel;

        [SerializeField]
        private CounterView view;


        private void Awake()
        {
            scoreEventChannel.ScoreUpdated += UpdateView;
            scoreEventChannel.ScoreReset += UpdateView;
        }

        private void UpdateView()
        {
            view.UpdateCount("0");
        }


        private void UpdateView(int value)
        {
            view.UpdateCount(value.ToString());
        }


        private void OnDisable()
        {
            scoreEventChannel.ScoreUpdated -= UpdateView;
            scoreEventChannel.ScoreReset -= UpdateView;
        }
    }
}