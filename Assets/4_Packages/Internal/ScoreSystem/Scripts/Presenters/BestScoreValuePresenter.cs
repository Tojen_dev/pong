using Gameplay.Level;
using Packages.UI.Core.Count.Scripts;
using UnityEngine;

namespace Packages.Internal.ScoreSystem.Scripts.Presenters
{
    public class BestScoreValuePresenter : MonoBehaviour
    {
        [SerializeField]
        private ScoreEventChannel scoreEventChannel;

        [SerializeField]
        private CounterView view;

        private void Awake()
        {
            scoreEventChannel.BestScoreUpdated += UpdateText;
        }

        private void UpdateText(int value, bool loaded)
        {
            string text = value.ToString();
            view.UpdateCount(text);
        }


        private void OnDisable()
        {
            scoreEventChannel.BestScoreUpdated -= UpdateText;
        }
    }
}