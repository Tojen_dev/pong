﻿using Cam.Scripts;
using DG.Tweening;
using Packages.Cam.Scripts.Actions;
using UnityEngine;

namespace Packages.Cam.Scripts
{
    public class CameraTransitionController : CameraAction
    {
        [SerializeField]
        private float offset;

        [SerializeField]
        private bool calculateOffset;


        public override void Initialize(CameraController controller)
        {
            if (calculateOffset)
                offset = transform.position.y;

            //LevelController.OnFloorCleared += Move;
        }

        private void Move(Vector3 position)
        {
            transform.DOMoveY(position.y + offset, .5f);
        }
    }
}