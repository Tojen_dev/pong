﻿using DG.Tweening;
using UnityEngine;

namespace Packages.Cam.Scripts.Transitions
{
    public class ToCrashTransition : MonoBehaviour
    {
        [SerializeField]
        private float moveDuration;

        public Tween MoveSmooth(Transform camera, float x)
        {
            return camera.DOMoveX(x, moveDuration).SetUpdate(true);
        }

        public void Reset(Transform camera)
        {
            camera.DOMoveX(0, moveDuration);
        }
    }
}