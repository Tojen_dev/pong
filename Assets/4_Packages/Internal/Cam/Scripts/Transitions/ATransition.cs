﻿using UnityEngine;

namespace Packages.Cam.Scripts.Transitions
{
    public abstract class ATransition : MonoBehaviour
    {
        public abstract void MoveInstant(Transform camera, Transform target);
        public abstract void MoveSmooth(Transform camera, Transform target);
    }
}