﻿using DG.Tweening;
using Structures;
using UnityEngine;

namespace _4_Packages.Internal.Cam.Scripts.Transitions
{
    public class CameraTransformTransition : MonoBehaviour
    {
        [Header("Durations:")]
        [SerializeField]
        private float moveDuration;

        [SerializeField]
        private float rotateDuration;


        public Sequence Run(Transform camera, TransformValues targetInfo)
        {
            return DOTween.Sequence()
                .Append(camera.DOMove(targetInfo.Position, moveDuration))
                .Join(camera.DORotateQuaternion(targetInfo.Rotation, rotateDuration));
        }
    }
}