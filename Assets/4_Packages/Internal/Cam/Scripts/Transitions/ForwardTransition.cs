﻿using System;
using DG.Tweening;
using Extensions;
using UnityEngine;

namespace Packages.Cam.Scripts.Transitions
{
    public class ForwardTransition : ATransition
    {
        public event Action Started;
        public event Action Finished;

        [Header("Durations:")]
        [SerializeField]
        private float moveDuration;

        [SerializeField]
        private float rotateDuration;

        [Header("Position")]
        [SerializeField]
        private Vector3 forwardOffset;

        [SerializeField]
        private float rotateAroundAngle;


        [Header("Move look:")]
        [SerializeField]
        private float lookAtSpeed;

        [SerializeField]
        private float lookAtAcceleration;


        private float _speed;

        public override void MoveInstant(Transform camera, Transform target)
        {
            Started?.Invoke();

            camera.position = GetPosition(target);
            camera.rotation = GetRotation(camera.position, target.position);

            Finished?.Invoke();
        }

        public override void MoveSmooth(Transform camera, Transform target)
        {
            Started?.Invoke();

            _speed = 0;

            Vector3 position = GetPosition(target);
            //Quaternion rotation = GetRotation(position, target.position);

            DOTween.Sequence()
                .Append(camera.DOMove(position, moveDuration)
                    .OnUpdate(() => LookAtSmooth(camera, target)))
                //    .Append(camera.DORotateQuaternion(rotation, rotateDuration))
                .OnComplete(() =>
                {
                    camera.LookAt(target);
                    Finished?.Invoke();
                });
        }


        private Vector3 GetPosition(Transform target)
        {
            Vector3 center = target.position;
            Vector3 position = center + target.forward * forwardOffset.z;

            if (rotateAroundAngle != 0)
                position = TransformExtension.RotateVector3AroundY(center, position, rotateAroundAngle);

            position.y = target.position.y + forwardOffset.y;

            return position;
        }

        private Quaternion GetRotation(Vector3 cameraPosition, Vector3 targetPosition)
        {
            Vector3 relativePos = targetPosition - cameraPosition;
            Vector3 eulerAngles = Quaternion.LookRotation(relativePos, Vector3.up).eulerAngles;
            //eulerAngles.x = lookAtAngleX;

            Quaternion rotation = Quaternion.Euler(eulerAngles);

            return rotation;
        }

        private void LookAtSmooth(Transform camera, Transform target)
        {
            _speed += lookAtAcceleration * Time.deltaTime;
            
            if (_speed > lookAtSpeed)
                _speed = lookAtSpeed;
            
            Vector3 relativePos = target.position - camera.position;
            Quaternion targetRotation = Quaternion.LookRotation(relativePos, Vector3.up);

            camera.rotation = Quaternion.Lerp(camera.rotation, targetRotation, _speed * Time.deltaTime);
        }
    }
}