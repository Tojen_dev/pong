﻿using Gameplay;
using UnityEngine;
using Utilities;

namespace Packages.Cam.Scripts.Transitions
{
    public class CameraTransitionTest : MonoBehaviour
    {
        [SerializeField]
        private Transform camera;

        [SerializeField]
        private Transform target;

        [Space]
        [SerializeField]
        private ATransition transition;

        
        private TransformSaver _transformSaver;

        
        private void Start()
        {
            _transformSaver = new TransformSaver();
            _transformSaver.Save(camera);

            target = FindTarget();
        }

        private Transform FindTarget()
        {
            // var players = FindObjectsOfType<Player>();
            // for (var i = 0; i < players.Length; i++)
            // {
            //     if (players[i].IsPlayer)
            //         return players[i].Center;
            // }

            return null;
        }


        public void UseInstant()
        {
            transition.MoveInstant(camera, target);
        }

        public void UseSmooth()
        {
            transition.MoveSmooth(camera, target);
        }


        public void Reset()
        {
            _transformSaver.Load(camera);
        }
    }
}