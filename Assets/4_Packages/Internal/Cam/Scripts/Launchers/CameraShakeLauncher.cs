﻿using Cam.Scripts.Actions;
using DG.Tweening;
using UnityEngine;

namespace Cam.Scripts.Launchers
{
    public class CameraShakeLauncher : MonoBehaviour
    {
        [SerializeField]
        private ShakeTween shake;

        private Tween _launched;


        public Tween Run()
        {
            _launched?.Kill();
            _launched = shake.Play(transform);

            return _launched;
        }

        public void Reset()
        {
            _launched.Kill();
            transform.localPosition = Vector3.zero;
        }
    }
}