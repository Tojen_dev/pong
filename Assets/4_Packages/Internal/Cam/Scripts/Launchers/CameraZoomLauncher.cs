﻿using Cam.Scripts.Actions;
using DG.Tweening;
using UnityEngine;

namespace Cam.Scripts.Launchers
{
    public class CameraZoomLauncher : MonoBehaviour
    {
        [SerializeField]
        private ZoomTween zoom;

        private Tween _launched;


        public Tween Run()
        {
            var transform = GetComponent<Transform>();
            
            _launched?.Kill();
            _launched = zoom.Run(transform, -transform.forward.normalized);

            return _launched;
        }

        public void Reset()
        {
            _launched.Kill();
            
            transform.localPosition = Vector3.zero;
        }
    }
}