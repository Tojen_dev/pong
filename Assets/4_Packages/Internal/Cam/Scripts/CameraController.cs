﻿using Packages.Cam.Scripts;
using Packages.Cam.Scripts.Actions;
using UnityEngine;
using Utilities;
using Zenject;

namespace Cam.Scripts
{
    public class CameraController : MonoBehaviour, IInitializable
    {
        [Header("Actions:")]
        [SerializeField]
        private CameraAction[] initialize;

        [SerializeField]
        private CameraAction[] tick;

        [SerializeField]
        private CameraAction[] fixedTick;


        public Transform Transform { get; private set; }
        public Camera Camera { get; private set; }


        public Vector3 LocalPosition
        {
            get => Transform.localPosition;
            set => Transform.localPosition = value;
        }

        public Vector3 Position => Transform.position;


        private TransformSaver _transformSaver;
        private bool _canTick;


        private void Awake()
        {
            Initialize();
        }

        public void Initialize()
        {
            Camera = GetComponent<Camera>();
            Transform = transform;

            for (int i = 0; i < initialize.Length; i++)
                initialize[i].Initialize(this);

            _transformSaver = new TransformSaver();
            _transformSaver.SaveLocal(Transform);
        }


        private void Update()
        {
            if (_canTick == false)
                return;
            
            for (var i = 0; i < tick.Length; i++)
                tick[i].Tick(this);
        }

        private void FixedUpdate()
        {
            if (_canTick == false)
                return;
            
            for (var i = 0; i < fixedTick.Length; i++)
                fixedTick[i].Tick(this);
        }

        
        public void StartFollow()
        {
            _canTick = true;
        }

        public void StopFollow()
        {
            _canTick = false;
        }

        public void Reset()
        {
            _canTick = true;
            _transformSaver.LoadLocal(Transform);
        }
    }
}