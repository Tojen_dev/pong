﻿using Cam.Scripts;
using UnityEngine;

namespace Packages.Cam.Scripts.Actions
{
    public class CameraAction : MonoBehaviour
    {
        public virtual void Initialize(CameraController controller)
        {
        }

        public virtual void Tick(CameraController controller)
        {
        }

        public virtual void Reset()
        {
            
        }
    }
}