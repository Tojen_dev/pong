﻿using UnityEngine;

namespace Packages.Cam.Scripts.Actions.Move.Translations
{
    [CreateAssetMenu(menuName = "Camera/Move/Translation/Fixed")]
    public class FixedTranslation : FollowTranslationPattern
    {
        public override Vector3 Use(Vector3 position, Vector3 target)
        {
            return target;
        }
    }
}