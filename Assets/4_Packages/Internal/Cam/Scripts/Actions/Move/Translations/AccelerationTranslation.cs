﻿using UnityEngine;

namespace Packages.Cam.Scripts.Actions.Move.Translations
{
    [CreateAssetMenu(menuName = "Camera/Move/Translation/Acceleration")]
    public class AccelerationTranslation : FollowTranslationPattern
    {
        [SerializeField]
        private float acceleration;

        [SerializeField]
        private float maxSpeed;

        [SerializeField]
        private float maxDistance;


        private float _speed;


        public override Vector3 Use(Vector3 position, Vector3 targetPosition)
        {
            UpdateSpeed();

            if (TryResetSpeed(position, targetPosition))
                return targetPosition;
            
            Vector3 result = Vector3.MoveTowards(position, targetPosition, _speed * Time.deltaTime);
            if ((result - targetPosition).sqrMagnitude > Mathf.Pow(maxDistance, 2))
            {
                Vector3 dir = (result - targetPosition).normalized;
                result = targetPosition - dir * maxDistance;
            }

            return result;
        }


        private bool TryResetSpeed(Vector3 position, Vector3 targetPosition)
        {
            if (targetPosition == position)
            {
                _speed = 0;
                return true;
            }

            return false;
        }


        private void UpdateSpeed()
        {
            if (_speed >= maxSpeed)
                return;

            _speed += acceleration * Time.deltaTime;
            if (_speed > maxSpeed)
                _speed = maxSpeed;
        }


        public void Reset()
        {
            _speed = 0;
        }
    }
}