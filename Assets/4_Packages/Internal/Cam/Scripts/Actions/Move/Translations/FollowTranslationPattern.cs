﻿using UnityEngine;

namespace Packages.Cam.Scripts.Actions.Move.Translations
{
    public abstract class FollowTranslationPattern : ScriptableObject
    {
        public abstract Vector3 Use(Vector3 position, Vector3 target);

        
        public virtual void Reset()
        {
        }
    }
}