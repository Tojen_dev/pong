﻿using UnityEngine;

namespace Packages.Cam.Scripts.Actions.Move.Constraints
{
    [CreateAssetMenu(menuName = "Camera/Move/Constraint/")]
    public abstract class FollowConstraint : ScriptableObject
    {
        public abstract Vector3 Use(Vector3 initial, Vector3 target);
    }
}