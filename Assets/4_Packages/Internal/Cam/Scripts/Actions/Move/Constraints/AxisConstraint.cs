﻿using UnityEngine;

namespace Packages.Cam.Scripts.Actions.Move.Constraints
{
    [CreateAssetMenu(menuName = "Camera/Move/Constraint/Axis")]
    public class AxisConstraint : FollowConstraint
    {
        [SerializeField]
        private bool freezeX;

        [SerializeField]
        private bool freezeY;

        [SerializeField]
        private bool freezeZ;


        public override Vector3 Use(Vector3 initial, Vector3 target)
        {
            Vector3 position = target;

            if (freezeX)
                position.x = initial.x;

            if (freezeY)
                position.y = initial.y;

            if (freezeZ)
                position.y = initial.z;

            return position;
        }
    }
}