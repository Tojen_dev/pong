﻿using Cam.Scripts;
using Packages.Cam.Scripts.Actions.Move.Constraints;
using Packages.Cam.Scripts.Actions.Move.Translations;
using UnityEngine;

namespace Packages.Cam.Scripts.Actions.Move
{
    public class CameraFollow : CameraAction
    {
        [SerializeField]
        private Transform target;

        [Space]
        [SerializeField]
        private bool calculateOffset;

        [SerializeField]
        private Vector3 offset;

        [Space]
        [SerializeField]
        private FollowTranslationPattern translationPattern;

        [SerializeField]
        private FollowConstraint constraint;


        public override void Initialize(CameraController controller)
        {
            if (calculateOffset)
                offset = target.position - controller.Transform.position;
        }

        public override void Tick(CameraController controller)
        {
            Vector3 targetPosition = FindTargetPosition(controller);
            controller.Transform.position = translationPattern.Use(controller.Position, targetPosition);
        }

        private Vector3 FindTargetPosition(CameraController controller)
        {
            Vector3 targetPosition = target.position - offset;

            if (constraint)
                targetPosition = constraint.Use(controller.Position, targetPosition);

            return targetPosition;
        }
    }
}