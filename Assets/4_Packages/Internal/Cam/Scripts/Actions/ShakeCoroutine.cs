﻿using System.Collections;
using UnityEngine;

namespace Packages.Cam.Scripts.Actions
{
    public class ShakeCoroutine : MonoBehaviour
    {
        [SerializeField]
        private float power;

        [SerializeField]
        private float duration;


        public void Play(Transform shakeTransform)
        {
            StartCoroutine(PlayWaiter(shakeTransform));
        }

        public IEnumerator PlayWaiter(Transform shakeTransform)
        {
            float elapsed = 0;

            Vector3 originalPosition = shakeTransform.position;
            Vector3 shakePosition = shakeTransform.position;

            while (elapsed < duration)
            {
                float x = Random.Range(-1f, 1f) * power;
                float y = Random.Range(-1f, 1f) * power;

                shakePosition.x = originalPosition.x + x;
                shakePosition.y = originalPosition.y + y;

                shakeTransform.position = shakePosition;

                elapsed += Time.deltaTime;
                yield return null;
            }

            shakeTransform.position = originalPosition;
        }
    }
}