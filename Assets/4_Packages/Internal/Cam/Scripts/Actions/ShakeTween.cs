﻿using DG.Tweening;
using UnityEngine;

namespace Cam.Scripts.Actions
{
    [CreateAssetMenu(fileName = "Zoom",menuName = "Camera/Actions/Shake")]
    public class ShakeTween : ScriptableObject
    {
        public float shakeDuration;
        public float shakeStrength;
        public int shakeVibrato;


        public Tween Play(Transform transform)
        {
            Vector3 initPosition = transform.position;
            return transform.DOShakePosition(shakeDuration, shakeStrength, shakeVibrato)
                .OnComplete(() => { transform.position = initPosition; });
        }
    }
}