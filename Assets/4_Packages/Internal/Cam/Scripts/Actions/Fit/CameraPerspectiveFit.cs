﻿using System;
using Cam.Scripts;
using UnityEngine;

namespace Packages.Cam.Scripts.Actions.Fit
{
    public class CameraPerspectiveFit : CameraAction
    {
        [SerializeField]
        private SizeInfo tall = new SizeInfo {ratio = 2.164251f};

        [SerializeField]
        private SizeInfo wide = new SizeInfo {ratio = 1.778667f};


        public override void Initialize(CameraController controller)
        {
            float ratio = (float) Screen.height / Screen.width;
            float percent = Mathf.InverseLerp(wide.ratio, tall.ratio, ratio);
            Vector3 position = Vector3.Lerp(wide.position, tall.position, percent);

            controller.Transform.position = position;
        }

        public override void Reset()
        {
            
        }

        #region Inner

        [Serializable]
        public class SizeInfo
        {
            public float ratio;
            public Vector3 position;
        }

        #endregion
    }
}