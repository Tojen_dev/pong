﻿using System;
using Cam.Scripts;
using UnityEngine;

namespace Packages.Cam.Scripts.Actions.Fit
{
    public class CameraOrthographicFit : CameraAction
    {
        [SerializeField]
        private SizeInfo tall = new SizeInfo {ratio = 2.164251f};

        [SerializeField]
        private SizeInfo wide = new SizeInfo {ratio = 1.778667f};


        public override void Initialize(CameraController controller)
        {
            float ratio = (float) Screen.height / Screen.width;
            float percent = Mathf.InverseLerp(wide.ratio, tall.ratio, ratio);
            float size = Mathf.Lerp(wide.size, tall.size, percent);

            controller.Camera.orthographicSize = size;
        }

        #region Inner

        [Serializable]
        public class SizeInfo
        {
            public float ratio;
            public float size;
        }

        #endregion
    }
}