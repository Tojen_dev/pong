using DG.Tweening;
using UnityEngine;

namespace Cam.Scripts.Actions
{
    [CreateAssetMenu(fileName = "Zoom", menuName = "Camera/Actions/Zoom")]
    public class ZoomTween : ScriptableObject
    {
        [SerializeField]
        private bool isSpeedBased;

        [SerializeField]
        private float duration;

        [SerializeField]
        private float additionalDistance;

        [SerializeField]
        private Ease ease;


        public Tween Run(Transform transform, Vector3 direction)
        {
            Vector3 targetPosition = transform.localPosition + direction * additionalDistance;
            return transform.DOLocalMove(targetPosition, duration)
                .SetSpeedBased(isSpeedBased)
                .SetEase(ease);
        }

        public Tween Run(Transform transform)
        {
            Vector3 targetPosition = transform.localPosition - transform.forward * additionalDistance;
            return transform.DOLocalMove(targetPosition, duration)
                .SetSpeedBased(isSpeedBased)
                .SetEase(ease);
        }
    }
}