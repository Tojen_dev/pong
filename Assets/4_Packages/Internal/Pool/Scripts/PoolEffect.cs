﻿using System.Collections;
using UnityEngine;

namespace Pool
{
    public class PoolEffect : PoolObject
    {
        [SerializeField]
        private new ParticleSystem particleSystem;

        private IEnumerator _waiter;


        public void Show()
        {
            if (gameObject.activeInHierarchy)
                Restart();
            else
                gameObject.SetActive(true);

            StartHideWaiter();
        }

        public void Hide()
        {
            StopHideWaiter();
            Push();
        }

        private void Restart()
        {
            particleSystem.time = 0;
            StopHideWaiter();
        }


        public void UpdateColor(Color32 color)
        {
            var main = particleSystem.main;
            main.startColor = new ParticleSystem.MinMaxGradient(color);
        }


        public override void OnPop()
        {
            base.OnPop();
            StartHideWaiter();
        }


        private void StartHideWaiter()
        {
            _waiter = HideWaiter();
            StartCoroutine(_waiter);
        }

        private void StopHideWaiter()
        {
            if (_waiter != null)
                StopCoroutine(_waiter);
        }

        private IEnumerator HideWaiter()
        {
            yield return new WaitForSeconds(particleSystem.main.duration);
            Push();
        }
    }
}