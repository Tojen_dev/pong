﻿using System.Collections.Generic;
using UnityEngine;

namespace Pool
{
    public class PoolManager : MonoBehaviour
    {
        public static PoolManager Instance;

        [SerializeField]
        private Transform freeContainer;

        [Header("Preload:")]
        [SerializeField]
        private Transform preloadContainer;

        [Space]
        [SerializeField]
        private PoolObject[] preloadObjects;


        private Dictionary<string, Queue<PoolObject>> _objects;

        public Transform FreeContainer => freeContainer ?? transform;


        private void Awake()
        {
            Instance = this;
            _objects = new Dictionary<string, Queue<PoolObject>>();

            Preload();
        }

        private void Preload()
        {
            Vector3 position = preloadContainer.position;
            Quaternion rotation = Quaternion.identity;

            for (int i = 0; i < preloadObjects.Length; i++)
            {
                PoolObject poolObject = CreateObject(preloadObjects[i], position, rotation, preloadContainer);
                poolObject.Push();
            }
        }


        public void Load(PoolObject prefab, int count, Transform parent)
        {
            Vector3 position = Vector3.zero;
            Quaternion rotation = Quaternion.identity;

            for (int i = 0; i < count; i++)
            {
                PoolObject poolObject = CreateObject(prefab, position, rotation, parent);
                poolObject.Push();
            }
        }


        public void Push(string groupKey, PoolObject value)
        {
            value.OnPush();

            if (value.MoveToContainerOnHide)
                value.Transform.SetParent(FreeContainer);

            if (!_objects.ContainsKey(groupKey))
            {
                _objects.Add(groupKey, new Queue<PoolObject>());
            }

            _objects[groupKey].Enqueue(value);
        }

        public T PopOrCreate<T>(T prefab, Transform parent) where T : PoolObject
        {
            return PopOrCreate(prefab, Vector3.zero, Quaternion.identity, parent);
        }

        public T PopOrCreate<T>(T prefab, Vector3 position, Quaternion rotation) where T : PoolObject
        {
            return PopOrCreate(prefab, position, rotation, freeContainer);
        }

        public T PopOrCreate<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent) where T : PoolObject
        {
            T result = Pop<T>(prefab.Group);

            if (result == null)
            {
                result = CreateObject<T>(prefab, position, rotation, parent);
            }
            else
            {
                result.SetTransform(position, rotation, parent);
            }

            result.OnPop();

            return result;
        }

        private T Pop<T>(string groupKey) where T : PoolObject
        {
            T result = null;

            if (_objects.ContainsKey(groupKey) && _objects[groupKey].Count > 0)
            {
                result = (T)_objects[groupKey].Dequeue();
            }

            return result;
        }

        private T CreateObject<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent)
            where T : PoolObject
        {
            var instance = Instantiate(prefab.gameObject, position, rotation, parent);
            T result = instance.GetComponent<T>();
            result.name = prefab.name;
            return result;
        }

        public void ClearList<T>(List<T> list) where T : PoolObject
        {
            if (list == null)
                return;

            for (var i = 0; i < list.Count; i++)
                list[i].Push();

            list.Clear();
        }

        public void HideObject<T>(T item, List<T> list) where T : PoolObject
        {
            if (item == null || list == null)
                return;

            list.Remove(item);
            item.Push();
        }
    }
}