﻿using System.Collections;
using UnityEngine;

namespace Pool.Scripts
{
    public class PoolAnimation : PoolObject
    {
        [SerializeField]
        private Animator animator;


        public void Show()
        {
            gameObject.SetActive(true);
            StartCoroutine(HideWaiter());
        }

        public void Hide()
        {
            StopHideWaiter();
            Push();
        }


        public override void OnPop()
        {
            base.OnPop();
            StartCoroutine(HideWaiter());
        }


        private IEnumerator HideWaiter()
        {
            yield return null;
            yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
            Push();
        }

        private void StopHideWaiter()
        {
            StopAllCoroutines();
        }
    }
}
