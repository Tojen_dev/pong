﻿using UnityEngine;

namespace Pool
{
    public class PoolObject : MonoBehaviour
    {
        [SerializeField]
        private bool moveToContainerOnHide = true;

        public string Group => name;

        public Transform Transform => _transform;
        public GameObject GameObject => _gameObject;
        public bool MoveToContainerOnHide => moveToContainerOnHide;


        public Vector3 Position
        {
            get => _transform.position;
            set => _transform.position = value;
        }

        public Quaternion Rotation
        {
            get => _transform.rotation;
            set => _transform.rotation = value;
        }


        private Transform _transform;
        private GameObject _gameObject;


        protected virtual void Awake()
        {
            _transform = transform;
            _gameObject = gameObject;
        }


        public void SetTransform(Vector3 position, Quaternion rotation, Transform parent)
        {
            _transform.SetParent(parent);
            _transform.position = position;
            _transform.rotation = rotation;
        }


        public virtual void OnPop()
        {
            _gameObject.SetActive(true);
        }

        public virtual void OnPush()
        {
            _gameObject.SetActive(false);
        }

        public virtual void Push()
        {
            PoolManager.Instance.Push(Group, this);
        }

        public virtual void Reset()
        {
        }
    }
}