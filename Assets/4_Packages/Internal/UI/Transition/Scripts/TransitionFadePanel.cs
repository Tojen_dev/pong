﻿using System;
using System.Collections;
using UnityEngine;

namespace Ui.Components
{
    public class TransitionFadePanel : MonoBehaviour
    {
        public event Action OnFade;

        [SerializeField]
        private Animator animator;


        public void Show()
        {
            gameObject.SetActive(true);
            StartCoroutine(HideWaiter());
        }

       private IEnumerator HideWaiter()
        {
            yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length / 2);
            OnFade?.Invoke();
            yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length / 2);
            gameObject.SetActive(false);
        }
    }
}