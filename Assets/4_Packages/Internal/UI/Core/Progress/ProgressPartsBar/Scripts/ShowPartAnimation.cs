﻿using DG.Tweening;
using UnityEngine;

namespace Packages.UI.Progress.ProgressPartsBar.Scripts
{
    [CreateAssetMenu(menuName = "Animation/UI/ShowProgressPart")]
    public class ShowPartAnimation : ScriptableObject
    {
        public float endValue;
        public float duration;

        
        public Tween Use(Transform part)
        {
            return part.DOScale(endValue, duration);
        }
    }
}