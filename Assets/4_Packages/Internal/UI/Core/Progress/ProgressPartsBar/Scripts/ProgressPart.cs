﻿using Pool;
using UnityEngine;

namespace Packages.UI.Progress.ProgressPartsBar.Scripts
{
    public class ProgressPart : PoolObject
    {
      
        [SerializeField]
        private GameObject completeImage;


        public void Complete(ShowPartAnimation animation)
        {
            completeImage.SetActive(true);
            completeImage.transform.localScale = Vector3.zero;
            
            animation.Use(completeImage.transform);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Reset()
        {
            completeImage.SetActive(false);
        }
    }
}