﻿using System.Collections.Generic;
using Extensions;
using Pool;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Packages.UI.Progress.ProgressPartsBar.Scripts
{
    public class ProgressPartsBar : MonoBehaviour
    {
        [Header("Level texts:")]
        [SerializeField]
        private TextMeshProUGUI currentLevelText;

        [SerializeField]
        private TextMeshProUGUI nextLevelText;

        [Header("Animations:")]
        [SerializeField]
        private ShowPartAnimation showPartAnimation;

        [Header("Parts:")]
        [SerializeField]
        private RectTransform partsContainer;

        [SerializeField]
        private HorizontalLayoutGroup horizontalLayout;

        [SerializeField]
        private ProgressPart part;

        [Space]
        [SerializeField]
        private List<ProgressPart> parts;


        public void LoadLevel(int level, int steps)
        {
            UpdateLevels(level);
            FillParts(steps);
        }

        public void FillParts(int steps)
        {
            int count = Mathf.Max(steps, parts.Count);

            //Create parts
            for (int i = 0; i < count; i++)
            {
                if (steps <= i)
                    parts[i].Hide();
                else
                {
                    if (i >= parts.Count)
                        CreatePart(i);
                    else
                        parts[i].Show();
                }
            }

            //Update width
            float width = steps * part.GetComponent<RectTransform>().sizeDelta.x +
                          (steps - 1) * horizontalLayout.spacing;
            partsContainer.SetSizeDeltaX(width);
        }


        private void CreatePart(int index)
        {
            parts[index] = PoolManager.Instance.PopOrCreate(part, partsContainer);
        }


        public void UpdateProgress(int step)
        {
            if (step <= 0 || step > parts.Count)
                return;

            parts[step - 1].Complete(showPartAnimation);
        }

        public void UpdateLevels(int current)
        {
            if (currentLevelText)
                currentLevelText.text = current.ToString();

            if (nextLevelText)
                nextLevelText.text = (current + 1).ToString();
        }


        public void Reset()
        {
            if (parts == null || parts.Count == 0)
                return;

            for (var i = 0; i < parts.Count; i++)
                parts[i].Reset();
        }
    }
}