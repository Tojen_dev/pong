﻿using UnityEngine;
using UnityEngine.UI;

namespace Packages.UI.Core.Progress.Fill.Scripts
{
    public class FillBar : MonoBehaviour
    {
        [SerializeField, Range(0, 5)]
        private float fillSpeed;


        [Header("Elements:")]
        [SerializeField]
        private Image fill;
        

        private bool _isActive;
        private float _targetFillValue;
        
        public Vector2 FillSize => fill.rectTransform.sizeDelta;


        private void Awake()
        {
            Reset();
        }


        private void Update()
        {
            TryFill();
        }

        private void TryFill()
        {
            if (!_isActive)
                return;

            fill.fillAmount = Mathf.MoveTowards(fill.fillAmount, _targetFillValue, fillSpeed * Time.deltaTime);

            if (fill.fillAmount == _targetFillValue)
                _isActive = false;
        }


        public void UpdateFill(float value)
        {
            _targetFillValue = value;
            _isActive = true;
        }

        public void UpdateFillColor(Color32 color)
        {
            fill.color = color;
        }


        public virtual void Reset()
        {
            _isActive = false;
            _targetFillValue = 0;

            fill.fillAmount = 0;
        }
    }
}