﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Packages.UI.Progress.Testing
{
    public class ProgressTest : MonoBehaviour
    {
        [SerializeField]
        private float incrementStep;

        [Header("Target:")]
        [SerializeField]
        private int totalCount;

        [SerializeField]
        private float[] percents;

        // [SerializeField]
        // private TargetProgressBar targetBar;


        private int _percentsIndex;
        private float _progress;


        private void Start()
        {
           // targetBar.Load(totalCount, percents);
            Reset();
        }

        public void Increment()
        {
            _progress += incrementStep;

            List<int> markIndexes = null;

            while (_percentsIndex < percents.Length && _progress >= percents[_percentsIndex])
            {
                if (markIndexes == null)
                    markIndexes = new List<int>();

                markIndexes.Add(_percentsIndex);
                _percentsIndex++;
            }

            //targetBar.UpdateFill(_progress, markIndexes);
        }

        public void Reset()
        {
            _percentsIndex = 0;
            _progress = 0;

           // targetBar.Reset();
        }
    }
}