﻿using Packages.UI.Core.Progress.Fill.Scripts;
using UnityEngine;

namespace Packages.UI.Core.Progress.ProgressBar.Scripts
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField]
        private FillBar fillBar;

        [SerializeField]
        private ProgressLevels levels;


        private GameObject _gameObject;


        private void Awake()
        {
            Initialize();
        }

        public void Initialize()
        {
            _gameObject = gameObject;

            fillBar = GetComponent<FillBar>();
            levels = GetComponent<ProgressLevels>();
        }


        public void UpdateFill(float percent)
        {
            fillBar.UpdateFill(percent);
        }

        public void UpdateLevels(int current)
        {
            if (levels)
                levels.SetText(current);
        }


        #region Show/Hide

        public void Show()
        {
            _gameObject.SetActive(true);
        }

        public void Hide()
        {
            _gameObject.SetActive(false);
        }

        #endregion

        public void Reset()
        {
            fillBar.Reset();
        }
    }
}