﻿using TMPro;
using UnityEngine;

namespace Packages.UI.Core.Progress.ProgressBar.Scripts
{
    public class ProgressLevels : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI currentLevelText;

        [SerializeField]
        private TextMeshProUGUI nextLevelText;

        
        public void SetText(int current)
        {
            if (currentLevelText)
                currentLevelText.text = current.ToString();

            if (nextLevelText)
                nextLevelText.text = (current + 1).ToString();
        }
    }
}