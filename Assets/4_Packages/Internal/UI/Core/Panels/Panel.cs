﻿using UnityEngine;
using Zenject;

namespace UI.Core.Panels
{
    public class Panel : MonoBehaviour, IInitializable
    {
        [SerializeField]
        private PanelName name;

        public PanelName Name => name;


        public virtual void Show() => gameObject.SetActive(true);
        public virtual void Hide() => gameObject.SetActive(false);


        public virtual void Initialize()
        {
        }
    }

    public enum PanelName
    {
        Menu,
        Game,
        GameOver,
        Victory,
        Shared,
        Shop
    }
}