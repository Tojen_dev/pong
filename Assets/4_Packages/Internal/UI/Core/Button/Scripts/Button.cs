﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UI.Core.Button.Scripts
{
    public class Button : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
    {
        [SerializeField]
        private bool interactable;

        [Header("Handlers:")]
        [SerializeField]
        private UnityEvent downHandler;

        [SerializeField]
        private UnityEvent upHandler;

        [Header("Elements:")]
        [SerializeField]
        private TextMeshProUGUI text;


        private Animator _animator;

        protected Animator Animator
        {
            get
            {
                if (_animator == null)
                    _animator = GetComponent<Animator>();

                return _animator;
            }
        }


        protected State state;

        private float NormalizedTime
        {
            get
            {
                float normalizedTime = _animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
                if (normalizedTime > 1)
                    normalizedTime = 1;

                return 1 - normalizedTime;
            }
        }

        private readonly int _up = Animator.StringToHash("Up");
        private readonly int _down = Animator.StringToHash("Down");
        private readonly int _show = Animator.StringToHash("Show");
        private readonly int _hide = Animator.StringToHash("Hide");
        private readonly int _isPressedHash = Animator.StringToHash("IsPressed");

        private void OnEnable()
        {
            if (state != State.Showing)
                Animator.Play(_up, 0, 1);
        }


        
        #region Show/Hide

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void ShowAnimated()
        {
            if (gameObject.activeInHierarchy)
                StartCoroutine(ShowAnimatedWaiter());
        }

        public IEnumerator ShowAnimatedWaiter()
        {
            if (state == State.Hiding)
                yield break;

            gameObject.SetActive(true);

            state = State.Showing;
            Lock();

            Animator.Play(_show, 0, 0);
            yield return null;
            yield return null;
            yield return new WaitForSecondsRealtime(Animator.GetCurrentAnimatorStateInfo(0).length);

            state = State.Idle;

            Unlock();
        }


        public void Hide()
        {
            ResetState();
            gameObject.SetActive(false);
        }

        public void HideAnimated()
        {
            StopAllCoroutines();

            if (gameObject.activeInHierarchy)
                StartCoroutine(HideAnimatedWaiter());
        }

        public IEnumerator HideAnimatedWaiter()
        {
            state = State.Hiding;

            Lock();
            Animator.Play(_hide, 0, 0);
            yield return null;
            yield return null;
            yield return new WaitForSecondsRealtime(Animator.GetCurrentAnimatorStateInfo(0).length);

            Hide();

            state = State.Idle;
        }

        #endregion
        
        #region PointerHandlers

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!interactable)
                return;

            state = State.Pressed;

            Animator.SetBool(_isPressedHash, state == State.Pressed);
            Animator.Play(_down, 0, NormalizedTime);

            downHandler?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (state != State.Pressed || !interactable)
                return;

            state = State.Idle;

            Animator.SetBool(_isPressedHash,  state == State.Pressed);
            Animator.Play(_up, 0, NormalizedTime);

            upHandler?.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (state == State.Pressed && interactable)
            {
                state = State.Idle;
                
                Animator.SetBool(_isPressedHash,  state == State.Pressed);
                Animator.Play(_up, 0, NormalizedTime);
            }
        }

        #endregion

        #region Interactable

        public void Lock()
        {
            interactable = false;
        }

        public void Unlock()
        {
            interactable = true;
        }

        #endregion


        public void SetText(string value)
        {
            text.text = value;
        }

        private void ResetState()
        {
            state = State.Idle;
        }


        protected enum State
        {
            Idle,
            Pressed,
            Showing,
            Hiding
        }
    }
}