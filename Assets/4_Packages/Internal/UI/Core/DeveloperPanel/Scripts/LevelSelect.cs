﻿using System;
using Level;
using States;
using TMPro;
using UnityEngine;
using Zenject;

namespace Packages.UI.DeveloperPanel.Scripts
{
    public class LevelSelect : MonoBehaviour
    {
        [Inject]
        private LevelsManager _levelsManager;

        [Inject]
        private LevelsLoader _levelsLoader;

        [Inject]
        private GameStatesManager _statesManager;


        [SerializeField]
        private TextMeshProUGUI levelNumber;

        [SerializeField]
        private TextMeshProUGUI levelName;

        [SerializeField]
        private TMP_InputField levelNumberInput;


        private int _level;


        private void OnEnable()
        {
            _level = _levelsManager.Level;
            UpdateValues();
        }
 

        public void Next()
        {
            _level++;

            if (_level > _levelsManager.TotalLevels)
                _level = 1;

            UpdateValues();
        }

        public void Previous()
        {
            _level--;

            if (_level == 0)
                _level = _levelsManager.TotalLevels;

            UpdateValues();
        }


        public void LevelNumberChanged(string value)
        {
            int.TryParse(value, out _level);
            _level = Mathf.Clamp(_level, 1, _levelsManager.TotalLevels);

            UpdateValues();
        }

        private void UpdateValues()
        {
            levelNumber.text = _level + " / " + _levelsManager.TotalLevels;
            levelName.text = _levelsManager.GetInfo(_level).name;
            levelNumberInput.text = _level.ToString();
        }

        public void Load()
        {
            _levelsManager.SetLevel(_level);
            _levelsLoader.LoadLevel();
            _statesManager.ToMenu();
        }
    }
}