﻿using System;
using System.Collections;
using Pool;
using UnityEngine;
using Utilities;

namespace Packages.UI.Core.Count.Scripts
{
    public class Counter : PoolObject
    {
        public event Action<Counter> Finished;


        [SerializeField]
        private int delayFrames;

        [SerializeField]
        private string prefix;

        [Space]
        [SerializeField]
        private CounterView view;

        private Animator _animator;


        protected override void Awake()
        {
            base.Awake();
            _animator = GetComponent<Animator>();
        }


        public void AddAnimated(int count)
        {
            StartCoroutine(AddAnimatedWaiter(count));
        }

        private IEnumerator AddAnimatedWaiter(int count)
        {
            int total = 1;
            view.Show(prefix + total);

            var delay = new WaitForFrames(delayFrames);

            while (total < count)
            {
                yield return delay;
                delay.Reset();

                total++;
                view.UpdateCount(prefix + total);
            }

            yield return null;
        }


        public void Show()
        {
            gameObject.SetActive(true);
            StartCoroutine(WaitForFinish());
        }

        private IEnumerator WaitForFinish()
        {
            yield return null;
            yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);

            Finished?.Invoke(this);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}