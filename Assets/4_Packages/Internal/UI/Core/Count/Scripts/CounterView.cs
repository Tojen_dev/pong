﻿using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace Packages.UI.Core.Count.Scripts
{
    public class CounterView : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI count;

        [SerializeField]
        private Animator animator;

        private readonly int _updateValueHash = Animator.StringToHash("UpdateValue");


        public void Show(string countValue)
        {
            count.text = countValue;
            gameObject.SetActive(true);
        }

        public void UpdateCount(string value)
        {
            count.text = value;
        }

        public void UpdateCountAnimated(string value)
        {
            count.text = value;
            animator.Play(_updateValueHash, 0, 0);
        }
    }
}