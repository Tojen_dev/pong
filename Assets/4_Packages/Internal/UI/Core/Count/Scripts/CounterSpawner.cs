﻿using System.Collections.Generic;
using Pool;
using UnityEngine;

namespace Packages.UI.Core.Count.Scripts
{
    public class CounterSpawner : MonoBehaviour
    {
        [SerializeField]
        private Counter plusCounter;

        [SerializeField]
        private Counter minusCounter;

        private ElementsOverlayPositionBinder _positionBinder;

        private Transform _transform;
        private List<Counter> _counters;


        public void Initialize()
        {
            _transform = transform;
            _counters = new List<Counter>();
            _positionBinder = new ElementsOverlayPositionBinder();

            //todo Subscribe
        }

        private void Update()
        {
            _positionBinder.UpdatePositions();
        }


        private void ShowPlusCounter(Transform point, int count)
        {
            ShowCounter(plusCounter, point, count);
        }

        private void ShowMinusCounter(Transform point, int count)
        {
            ShowCounter(minusCounter, point, count);
        }

        private void ShowCounter(Counter counter, Transform point, int count)
        {
            Vector3 position = Camera.main.WorldToScreenPoint(point.position);
            var instance = PoolManager.Instance.PopOrCreate(counter, position, Quaternion.identity, _transform);

            instance.Show();
            instance.AddAnimated(count);
            instance.Finished += HideCounter;

            _counters.Add(instance);
            _positionBinder.AddElement(instance.Transform, point);
        }

        private void HideCounter(Counter counter)
        {
            _counters.Remove(counter);
            _positionBinder.RemoveElement(counter.Transform);

            counter.Finished -= HideCounter;
            counter.Push();
        }

        private void Clear()
        {
            for (var i = 0; i < _counters.Count; i++)
            {
                _counters[i].Finished -= HideCounter;
                _counters[i].Push();
            }

            _counters.Clear();
            _positionBinder.Clear();
        }


        private void OnDisable()
        {
            Clear();
        }
    }
}