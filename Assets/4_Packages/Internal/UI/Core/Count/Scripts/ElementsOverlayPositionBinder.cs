﻿using System.Collections.Generic;
using UnityEngine;

namespace Packages.UI.Core.Count.Scripts
{
    public class ElementsOverlayPositionBinder
    {
        private Camera _camera;
        private Dictionary<Transform, Transform> _boundElements;


        public ElementsOverlayPositionBinder()
        {
            _camera = Camera.main;
            _boundElements = new Dictionary<Transform, Transform>();
        }


        public void UpdatePositions()
        {
            foreach (var pair in _boundElements)
            {
                if (pair.Key == null || pair.Value == null)
                    continue;

                Vector3 position = _camera.WorldToScreenPoint(pair.Value.position);
                pair.Key.position = position;
            }
        }

        public void AddElement(Transform element, Transform bindPoint)
        {
            if (_boundElements.ContainsKey(element) == false)
                _boundElements.Add(element, bindPoint);
        }

        public void RemoveElement(Transform element)
        {
            _boundElements.Remove(element);
        }

        public void Clear()
        {
            _boundElements.Clear();
        }
    }
}