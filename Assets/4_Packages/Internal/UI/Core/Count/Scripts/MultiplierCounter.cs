﻿using System.Collections;
using UnityEngine;
using Utilities;

namespace Packages.UI.Core.Count.Scripts
{
    public class MultiplierCounter : MonoBehaviour
    {
        [SerializeField]
        private int delayFrames;

        [SerializeField]
        private string prefix;

        [Space]
        [SerializeField]
        private CounterView view;

        private Animator _animator;

        private bool _isActive;
        private int _targetMultiplier;


        protected void Awake()
        {
            _animator = GetComponent<Animator>();

            //todo Subscribe
        }

        private void Deactivate()
        {
            _isActive = false;
        }

        private void Activate()
        {
            _isActive = true;

            Show();
            StartCoroutine(IncreaseMultiplierSmooth());
        }

        private void IncreaseMultiplier()
        {
            _targetMultiplier++;
        }


        private IEnumerator IncreaseMultiplierSmooth()
        {
            int multiplier = 1;
            var delay = new WaitForFrames(delayFrames);

            while (_isActive || multiplier != _targetMultiplier)
            {
                if (_targetMultiplier > multiplier)
                {
                    multiplier++;
                    view.UpdateCountAnimated(prefix + multiplier);

                    yield return delay;
                    delay.Reset();
                }
                else
                {
                    yield return null;
                }
            }
        }


        public void Show()
        {
            gameObject.SetActive(true);

            _targetMultiplier = 1;
            view.Show("");
        }


        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}