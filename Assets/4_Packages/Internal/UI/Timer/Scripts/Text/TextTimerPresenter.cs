using System.Collections;
using UnityEngine;

namespace Internal.UI.Timer.Text
{
    public class TextTimerPresenter : MonoBehaviour
    {
        [Header("Finish:")]
        [SerializeField]
        private string finishText;

        [SerializeField]
        private float delayAfterFinish;


        [Space]
        [SerializeField]
        private global::UI.Timer.Timer timer;

        [SerializeField]
        private TextTimerView view;


        private void Awake()
        {
            timer.Activated += ShowTime;
            timer.Tick += UpdateTime;
            timer.Finished += Finish;
            timer.Deactivated += HideView;
        }


        private void ShowTime(int value)
        {
            view.UpdateText("");
            view.Show();
        }

        private void UpdateTime(int value)
        {
            view.PlayAnimation(false);
            StartCoroutine(WaitForFrame(value));
        }

        private IEnumerator WaitForFrame(int value)
        {
            yield return null;
            view.UpdateText(value.ToString());
        }

        private void Finish()
        {
            view.UpdateText(finishText);
            view.PlayAnimation(true);

            StartCoroutine(HideWaiter());
        }

        private IEnumerator HideWaiter()
        {
            yield return new WaitForSeconds(delayAfterFinish);
            view.Hide();
        }


        private void HideView()
        {
            StopAllCoroutines();
            view.Hide();
        }


        private void OnDisable()
        {
            timer.Activated -= ShowTime;
            timer.Tick -= UpdateTime;
            timer.Finished -= Finish;
            timer.Deactivated -= HideView;
        }
    }
}