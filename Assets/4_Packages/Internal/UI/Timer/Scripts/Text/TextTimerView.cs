﻿using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace Internal.UI.Timer.Text
{
    public class TextTimerView : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI text;

        [SerializeField]
        private Animator animator;

        private readonly int _counterShow = Animator.StringToHash("CounterShow");
        private readonly int _goTextShow = Animator.StringToHash("GoTextShow");


        public void UpdateText(string value)
        {
            text.text = value;
            text.ForceMeshUpdate();
        }

        public void PlayAnimation(bool final)
        {
            if (final)
                animator.Play(_goTextShow, 0, 0);
            else
                animator.Play(_counterShow, 0, 0);

        }


        public void Show() => gameObject.SetActive(true);
        public void Hide() => gameObject.SetActive(false);
    }
}