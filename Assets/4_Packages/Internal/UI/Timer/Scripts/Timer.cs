﻿using System;
using System.Collections;
using Others.Activators;
using UnityEngine;

namespace UI.Timer
{
    public class Timer : MonoBehaviour, IActivatable
    {
        public event Action<int> Activated;
        public event Action<int> Tick;
        public event Action Finished;
        public event Action Deactivated;

        [SerializeField]
        private float tickDuration;

        [SerializeField]
        private int timeToStart;


        private IEnumerator _timer;


        public void Activate()
        {
            Deactivate();
            Activated?.Invoke(timeToStart);

            _timer = TickWaiter();
            StartCoroutine(TickWaiter());
        }


        private IEnumerator TickWaiter()
        {
            int time = timeToStart;
            var delay = new WaitForSeconds(tickDuration);

            while (time > 0)
            {
                Tick?.Invoke(time);
                yield return delay;
                time--;
            }

            Finished?.Invoke();
        }


        public void Deactivate()
        {
            if (_timer != null)
                StopCoroutine(_timer);
        }

        public void DeactivateWithCallback()
        {
            Deactivate();
            Deactivated?.Invoke();
        }
    }
}