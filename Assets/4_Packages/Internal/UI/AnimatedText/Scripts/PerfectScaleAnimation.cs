﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Packages.AnimatedText.Scripts
{
    [CreateAssetMenu(menuName = "Animation/UI/PerfectScale")]
    public class PerfectScaleAnimation : ScriptableObject
    {
        [SerializeField]
        private float from;

        [SerializeField]
        private Step[] steps;

        public Sequence Use(Transform item)
        {
            Sequence sequence = DOTween.Sequence().OnStart(() => item.localScale = Vector3.one * from);

            for (int i = 0; i < steps.Length; i++)
                sequence.Append(item.DOScale(steps[i].scale, steps[i].duration).SetEase(steps[i].ease));

            return sequence;
        }


        [Serializable]
        public class Step
        {
            public float scale;
            public float duration;

            [Space]
            public Ease ease;
        }
    }
}