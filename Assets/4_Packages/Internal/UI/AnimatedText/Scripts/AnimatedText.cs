using System;
using System.Collections;
using Pool;
using UnityEngine;

namespace Internal.UI.AnimatedText.Scripts
{
    public class AnimatedText : PoolObject
    {
        private Animator _animator;


        protected override void Awake()
        {
            base.Awake();
            _animator = GetComponent<Animator>();
        }


        public virtual void Show()
        {
            GameObject.SetActive(true);
            StartCoroutine(WaitForHide());
        }

        public override void OnPop()
        {
            base.OnPop();
            StartCoroutine(WaitForHide());
        }

        private IEnumerator WaitForHide()
        {
            yield return null;
            yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);

            Hide();
        }

        public void Hide()
        {
            StopAllCoroutines();
            GameObject.SetActive(false);
        }

        private void OnDisable()
        {
            Hide();
        }
    }
}