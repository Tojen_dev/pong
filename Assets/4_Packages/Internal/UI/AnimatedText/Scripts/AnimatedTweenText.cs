﻿using System;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Packages.AnimatedText.Scripts
{
    public class AnimatedTweenText : MonoBehaviour
    {
        [Header("Animation:")]
        [SerializeField]
        private PerfectScaleAnimation animation;


        private GameObject _gameObject;
        private Transform _innerText;

        private Sequence _animationSequence;


        private void Awake()
        {
            Initialize();
        }

        public virtual void Initialize()
        {
            _gameObject = gameObject;
            _innerText = GetComponentInChildren<Transform>();
        }

        public virtual void Show()
        {
            _gameObject.SetActive(true);
            _animationSequence = animation.Use(_innerText.transform)
                .OnComplete(Hide);
        }

        public void Hide()
        {
            _animationSequence?.Kill();
            _gameObject.SetActive(false);
        }
    }
}