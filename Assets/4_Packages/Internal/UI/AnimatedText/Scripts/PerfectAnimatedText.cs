﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Packages.AnimatedText.Scripts
{
    public class PerfectAnimatedText : MonoBehaviour
    {
        [SerializeField]
        private string[] words;

        [SerializeField]
        private Color32[] colors;

        [Header("Animation:")]
        [SerializeField]
        private PerfectScaleAnimation animation;


        [Header("Multiplier:")]
        [SerializeField]
        private Color32 multiplierColor;

        [SerializeField]
        private int multiplierSize;


        private TextMeshProUGUI _innerText;
        private GameObject _gameObject;
        private string _hexColor;

        private Sequence _animationSequence;


        private void Awake()
        {
            Initialize();
            Show();
        }

        public void Initialize()
        {
            _innerText = GetComponentInChildren<TextMeshProUGUI>();
            _gameObject = gameObject;

            _hexColor = ColorUtility.ToHtmlStringRGBA(multiplierColor);
        }

        public void Show()
        {
            _innerText.text = words[Random.Range(0, words.Length)];
            _innerText.color = colors[Random.Range(0, colors.Length)];

            _gameObject.SetActive(true);

            _animationSequence = animation.Use(_innerText.transform);
            _animationSequence.OnComplete(Hide);
        }

        public void Show(int multiplier)
        {
            Show();
            if (multiplier > 1)
            {
                _innerText.text += "<size=" + multiplierSize + "><color=#" + _hexColor + "> x" + multiplier +
                                   "</color></size>";
            }
        }


        public void Hide()
        {
            _animationSequence?.Kill();
            _gameObject.SetActive(false);
        }
    }
}