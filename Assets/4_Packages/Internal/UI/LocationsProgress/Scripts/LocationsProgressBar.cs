﻿using System.Collections.Generic;
using Extensions;
using Internal.UI.LocationsProgress.Scripts;
using Level;
using Pool;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Packages.UI.LocationsProgress.Scripts
{
    public class LocationsProgressBar : MonoBehaviour
    {
        [Header("Level texts:")]
        [SerializeField]
        private Image currentLocationImage;

        [SerializeField]
        private Image nextLocationImage;

        [Header("Colors:")]
        [SerializeField]
        private Color32 completedColor;

        [SerializeField]
        private Color32 currentColor;

        [SerializeField]
        private Color32 lockedColor;

        [Header("Parts:")]
        [SerializeField]
        private RectTransform partsContainer;

        [SerializeField]
        private HorizontalLayoutGroup horizontalLayout;

        [SerializeField]
        private LocationProgressPart[] partsPrefabs;

        [Space]
        [SerializeField]
        private List<LocationProgressPart> parts;


        private Dictionary<LevelType, LocationProgressPart> _partsPrefabs;


        public void BindParts()
        {
            _partsPrefabs = new Dictionary<LevelType, LocationProgressPart>();

            for (var i = 0; i < partsPrefabs.Length; i++)
            {
                if (_partsPrefabs.ContainsKey(partsPrefabs[i].Type) == false)
                    _partsPrefabs.Add(partsPrefabs[i].Type, partsPrefabs[i]);
            }
        }


        public void Load(LocationProgressInfo info)
        {
            FillLocationImages(info.LocationsIcons);
            FillParts(info.Steps, info.LevelIndex);
        }

        public void NextStep(int stepIndex)
        {
            parts[stepIndex - 1].SetColor(completedColor);
            parts[stepIndex].SetColor(currentColor);
        }

        private void FillLocationImages(Sprite[] locationIcons)
        {
            currentLocationImage.sprite = locationIcons[0];
            nextLocationImage.sprite = locationIcons[1];
        }

        private void FillParts(LevelType[] steps, int currentStepIndex)
        {
            ClearParts();

            for (int i = 0; i < steps.Length; i++)
            {
                LocationProgressPart part = CreatePart(steps[i]);

                Color32 color = GetColor(currentStepIndex, i);
                part.SetColor(color);
            }

            //Update width
            float width = steps.Length * partsPrefabs[0].GetComponent<RectTransform>().sizeDelta.x +
                          (steps.Length - 1) * horizontalLayout.spacing;
            partsContainer.SetSizeDeltaX(width);
        }

        public void ClearParts()
        {
            PoolManager.Instance.ClearList(parts);
        }

        private LocationProgressPart CreatePart(LevelType type)
        {
            LocationProgressPart prefab = _partsPrefabs[type];
            LocationProgressPart part = PoolManager.Instance.PopOrCreate(prefab, partsContainer);
            parts.Add(part);

            return part;
        }

        private Color32 GetColor(int currentStepIndex, int index)
        {
            if (index < currentStepIndex)
                return completedColor;

            if (index > currentStepIndex)
                return lockedColor;

            return currentColor;
        }
    }
}