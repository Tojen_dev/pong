﻿using Level;
using Pool;
using UnityEngine;
using UnityEngine.UI;

namespace Packages.UI.LocationsProgress.Scripts
{
    public class LocationProgressPart : PoolObject
    {
        [SerializeField]
        private Image image;

        [SerializeField]
        private LevelType levelType;


        public LevelType Type => levelType;
        

        public void SetColor(Color32 color)
        {
            image.color = color;
        }

        public void Show(bool value)
        {
            GameObject.SetActive(value);
        }
    }
}