using Level;
using UnityEngine;

namespace Internal.UI.LocationsProgress.Scripts
{
    public class LocationProgressInfo
    {
        public readonly int LevelIndex;
        
        public readonly LevelType[] Steps;
        public readonly Sprite[] LocationsIcons;

        
        public LocationProgressInfo(int levelIndex, LevelType[] steps, Sprite[] locationsIcons)
        {
            LevelIndex = levelIndex;
            Steps = steps;
            LocationsIcons = locationsIcons;
        }
    }
}