using Level;
using Packages.UI.LocationsProgress.Scripts;
using UnityEngine;
using Zenject;

namespace Internal.UI.LocationsProgress.Scripts
{
    public class LocationsProgressLoader : MonoBehaviour, IInitializable
    {
        [SerializeField]
        private LocationsProgressBar bar;

        [Inject]
        private LevelsManager _levelsManager;

        private LocationProgressInfo _info;


        public void Initialize()
        {
            bar.BindParts();
        }

        public void Load()
        {
            CreateLocationInfo();
            bar.Load(_info);
        }

        private void CreateLocationInfo()
        {
            Sprite[] icons = {_levelsManager.CurrentSet.icon, _levelsManager.NextSet.icon};
            LevelType[] steps = CreateSteps();

            _info = new LocationProgressInfo(_levelsManager.LevelIndex, steps, icons);
        }

        private LevelType[] CreateSteps()
        {
            LevelInfo[] currentSetLevels = _levelsManager.CurrentSet.levels;
            LevelType[] steps = new LevelType[currentSetLevels.Length];

            for (var i = 0; i < currentSetLevels.Length; i++)
                steps[i] = currentSetLevels[i].Type;

            return steps;
        }
    }
}