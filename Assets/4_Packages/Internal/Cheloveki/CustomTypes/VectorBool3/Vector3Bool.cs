using System;

namespace CustomTypes.VectorBool3
{
    [Serializable]
    public class Vector3Bool
    {
        public bool x;
        public bool y;
        public bool z;
    }
}