using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace CustomTypes.VectorBool3
{
    [CustomPropertyDrawer(typeof(Vector3Bool))]
    public class Vector3BoolDrawerUie : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            // Create property container element.
            var container = new VisualElement();

            // Create property fields.
            var amountField = new PropertyField(property.FindPropertyRelative("x"));
            var unitField = new PropertyField(property.FindPropertyRelative("y"));
            var nameField = new PropertyField(property.FindPropertyRelative("z"), "Fancy Name");

            // Add fields to the container.
            container.Add(amountField);
            container.Add(unitField);
            container.Add(nameField);

            return container;
        }
    }
}