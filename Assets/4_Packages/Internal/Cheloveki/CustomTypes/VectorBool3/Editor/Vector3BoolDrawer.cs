using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace CustomTypes.VectorBool3
{
    [CustomPropertyDrawer(typeof(Vector3Bool))]
    public class Vector3BoolDrawer : PropertyDrawer
    {
        float distanceToLabel = 18;
        float distanceToNextProperty = 14;
        private Vector2 rectPosition;
        private Rect positionRect;


        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            EditorGUIUtility.labelWidth = 14.0f;

            // Calculate rects
            rectPosition = new Vector2(position.x, position.y);
            positionRect = new Rect(position.x, position.y, 30, position.height);

            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            DrawProperty(property, "x", "X");
            DrawProperty(property, "y", "Y");
            DrawProperty(property, "z", "Z");

            // positionRect.position = rectPosition;
            // EditorGUI.PropertyField(positionRect, property.FindPropertyRelative("y"), new GUIContent());
            //EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("z")); //, new GUIContent("Z:"));

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }

        private void DrawProperty(SerializedProperty property, string propertyName, string propertyLabel)
        {
            EditorGUI.PropertyField(positionRect, property.FindPropertyRelative(propertyName), new GUIContent());
            rectPosition.x += distanceToLabel;
            positionRect.position = rectPosition;
            EditorGUI.LabelField(positionRect, propertyLabel);
            rectPosition.x += distanceToNextProperty;
            positionRect.position = rectPosition;
        }
    }
}

// EditorGUILayout.BeginHorizontal();
// GUILayout.Label("", GUILayout.Width(EditorGUIUtility.labelWidth));
// myFloat = EditorGUILayout.FloatField(myFloat);
// EditorGUILayout.EndHorizontal();